#include "cmbNucPartLibrary.h"
#include <boost/bind.hpp>
#include "cmbNucPinCell.h"
#include "cmbNucDuctCell.h"

cmbNucPartLibraryConnection::cmbNucPartLibraryConnection(cmbNucPartLibrary * pl)
:v(pl)
{
}

void cmbNucPartLibraryConnection::partDeleted(cmbNucPart * part)
{
  v->remove(part);
}

cmbNucPartLibrary::cmbNucPartLibrary()
: Connection(new cmbNucPartLibraryConnection(this))
{
}

cmbNucPartLibrary::~cmbNucPartLibrary()
{
  delete this->Connection;
  cmbNucPart::deleteObjs(this->Parts);
}

bool cmbNucPartLibrary::addPart(cmbNucPart* part)
{
  if(part == NULL) return false;
  QString name = part->getName();
  if(this->nameConflicts(name)) return false;
  QString label = part->getLabel();
  if(this->labelConflicts(label)) return false;
  this->Parts.push_back(part);
  this->NameToPart[name] = part;
  this->LabelToPart[label] = part;
  this->Connection->partChanged(NULL);
  QObject::connect(part->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                   this->Connection,      SIGNAL(partChanged(cmbNucPart*)));
  QObject::connect(part->getConnection(), SIGNAL(partDeleted(cmbNucPart*)),
                   this->Connection,      SLOT(partDeleted(cmbNucPart *)));
  return true;
}

bool cmbNucPartLibrary::addPart(cmbNucPart ** part)
{
  cmbNucPart * equiv = this->getEquivelent(*part);
  if(equiv != NULL)
  {
    delete *part;
    *part = equiv;
    return true;
  }
  return this->addPart(*part);
}

bool cmbNucPartLibrary::addPart(cmbNucPart * part,
                                std::map<QString, QString> & nameChange,
                                std::map<QString, QString> & labelChange)
{
  if(part == NULL) return false;
  boost::function<bool (QString)> fun = boost::bind(&cmbNucPartLibrary::nameConflicts, this, _1);
  QString n = cmbNucPart::getUnique(fun, part->getName(), true, false);
  fun = boost::bind(&cmbNucPartLibrary::labelConflicts, this, _1);
  QString l = cmbNucPart::getUnique(fun, part->getLabel(), true, false);

  if(n != part->getName()) nameChange[part->getName()] = n;
  if(l != part->getLabel()) labelChange[part->getLabel()] = l;
  part->setName(n);
  part->setLabel(l);
  return this->addPart(part);
}

cmbNucPart* cmbNucPartLibrary::GetPartByName(const QString &name)
{
  std::map<QString, cmbNucPart*>::const_iterator iter = this->NameToPart.find(name);
  if(iter == this->NameToPart.end()) return NULL;
  return iter->second;
}

cmbNucPart* cmbNucPartLibrary::GetPartByLabel(const QString &label)
{
  std::map<QString, cmbNucPart*>::const_iterator iter = this->LabelToPart.find(label);
  if(iter == this->LabelToPart.end()) return NULL;
  return iter->second;
}

cmbNucPart* cmbNucPartLibrary::GetPart(int pc) const
{
  if(static_cast<std::size_t>(pc) >= this->Parts.size()) return NULL;
  return this->Parts[pc];
}

std::size_t cmbNucPartLibrary::GetNumberOfParts() const
{
  return this->Parts.size();
}

void cmbNucPartLibrary::fillListJustName(std::vector< std::pair<QString, cmbNucPart *> > & l)
{
  for(std::vector<cmbNucPart*>::const_iterator it = this->Parts.begin();
      it != this->Parts.end(); ++it)
  {
    l.push_back(std::pair<QString, cmbNucPart *>((*it)->getName(), (*it)));
  }
}

void cmbNucPartLibrary::fillListNamePlusLabel(std::vector< std::pair<QString, cmbNucPart *> > & l)
{
  l.push_back(std::pair<QString, cmbNucPart *>("Empty Cell (xx)", NULL));
  for(std::vector<cmbNucPart*>::const_iterator it = this->Parts.begin();
      it != this->Parts.end(); ++it)
  {
    cmbNucPart *part = *it;
    l.push_back(std::pair<QString, cmbNucPart *>(part->getName() + " (" + part->getLabel() + ")",
                                                part));
  }
}

bool cmbNucPartLibrary::nameConflicts(QString n) const
{
  return this->NameToPart.find(n) != this->NameToPart.end();
}

bool cmbNucPartLibrary::labelConflicts(QString l) const
{
  return this->LabelToPart.find(l) != this->LabelToPart.end();
}

bool cmbNucPartLibrary::replaceName(QString oldN, QString newN)
{
  if(this->nameConflicts(newN)) return false;
  std::map<QString, cmbNucPart*>::iterator iter = this->NameToPart.find(oldN);
  if(iter == this->NameToPart.end()) return false;

  this->NameToPart[newN] = iter->second;
  this->NameToPart.erase(iter);
  return true;
}

bool cmbNucPartLibrary::replaceLabel(QString oldL, QString newL)
{
  if(this->labelConflicts(newL)) return false;
  std::map<QString, cmbNucPart*>::iterator iter = this->LabelToPart.find(oldL);
  if(iter == this->LabelToPart.end()) return false;

  this->LabelToPart[newL] = iter->second;
  this->LabelToPart.erase(iter);
  return true;
}

cmbNucPartLibrary * cmbNucPartLibrary::clone()
{
  cmbNucPartLibrary * result = new cmbNucPartLibrary();

  for(std::vector<cmbNucPart*>::const_iterator it = this->Parts.begin();
      it != this->Parts.end(); ++it)
  {
    cmbNucPart * part = (*it)->clone();
    result->addPart(part);
  }

  return result;
}

void cmbNucPartLibrary::removeFakeBoundaryLayer(QString blname)
{
  for(std::vector<cmbNucPart*>::const_iterator it = this->Parts.begin();
      it != this->Parts.end(); ++it)
  {
    cmbNucPart * part = *it;
    switch(part->GetType())
    {
      case CMBNUC_ASSY_DUCTCELL:
      {
        DuctCell * dc = dynamic_cast<DuctCell*>(part);
        dc->removeFakeBoundaryLayer(blname);
        break;
      }
      case CMBNUC_ASSY_PINCELL:
      {
        PinCell * pc = dynamic_cast<PinCell*>(part);
        pc->removeFakeBoundaryLayer(blname);
      }
    default:
      break;
    }
  }
}

void cmbNucPartLibrary::remove(cmbNucPart * part)
{
  if(cmbNucPart::removeObj(part, this->Parts, false))
  {
    this->NameToPart.erase(this->NameToPart.find(part->getName()));
    this->LabelToPart.erase(this->LabelToPart.find(part->getLabel()));
    this->Connection->partChanged(NULL);
  }
}

cmbNucPart * cmbNucPartLibrary::getEquivelent(cmbNucPart * part) const
{
  for(unsigned int i = 0; i < this->Parts.size(); ++i)
  {
    if(part->sameShape(this->Parts[i]))
    {
      return this->Parts[i];
    }
  }
  return NULL;
}

QString cmbNucPartLibrary::extractLabel(QString const& el) const
{
  QString seperator("(");
  QStringList ql = el.split( seperator );
  return ql[1].left(ql[1].size() -1);
}
