#include "cmbNucInpExporter.h"
#include "inpFileIO.h"
#include "cmbNucCore.h"
#include "cmbNucAssembly.h"
#include "cmbNucAssemblyLink.h"
#include "cmbNucPartLibrary.h"

#include <QFileInfo>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

#include <set>

typedef std::map< QString, std::set< Lattice::CellDrawMode > > LabelToModeMap;

cmbNucInpExporter
::cmbNucInpExporter()
{
}

void cmbNucInpExporter
::setCore(cmbNucCore * core)
{
  this->NuclearCore = core;
  this->updateCoreLayers(true);
}

bool cmbNucInpExporter
::exportInpFiles()
{
  QString coreName = this->NuclearCore->getExportFileName().c_str();
  if(coreName.isEmpty()) return false;

  //clone the pins and ducts.  These are needed for determining layers
  cmbNucPartLibrary * odl = this->NuclearCore->getDuctLibrary();
  cmbNucPartLibrary * pl = this->NuclearCore->getPinLibrary()->clone();
  cmbNucPartLibrary * dl = odl->clone();

  //split ducts if needed
  for(unsigned int i = 0; i < dl->GetNumberOfParts(); ++i)
  {
    DuctCell * dc = dl->GetPart<DuctCell>(i);
    if(!dc->isUsed()) continue;
    dc->uniformizeMaterialLayers();
    dc->splitDucts(this->coreLevelLayers.levels);
  }

  //split pins if needed
  for(unsigned int i = 0; i < pl->GetNumberOfParts(); ++i)
  {
    pl->GetPart<PinCell>(i)->splitPin(this->coreLevelLayers.levels);
  }

  QFileInfo coreinfo(coreName);

  LabelToModeMap cells = this->NuclearCore->getDrawModesForAssemblies();

  for(LabelToModeMap::const_iterator iter = cells.begin(); iter != cells.end(); ++iter)
  {
    cmbNucPart* part = this->NuclearCore->getFromLabel(iter->first);
    cmbNucAssembly* assembly = dynamic_cast<cmbNucAssembly*>(part);
    if(assembly != NULL)
    {
      assembly->setPath(coreinfo.dir().absolutePath().toStdString());
      std::set< Lattice::CellDrawMode > const& forms = iter->second;
      cmbNucAssembly* assemblyClone = assembly->clone(pl, dl);
      this->exportInpFile(assemblyClone, false, forms);
      delete assemblyClone;
    }
    else
    {
      // check to see if iter->first is a link
      cmbNucAssemblyLink * correspondingLink = dynamic_cast<cmbNucAssemblyLink*>(part);
      if (correspondingLink == NULL)
      {
        continue;
      }
      // correspondingLink is the link that this current iter is linked with
      // this assumes only 1 link can be had for any label

      QString linkTargetLabel = correspondingLink->getLink()->getLabel();
      cmbNucAssembly* linkTargetAssy =
                          dynamic_cast<cmbNucAssembly*>(NuclearCore->getFromLabel(linkTargetLabel));

      if (linkTargetAssy == NULL)
      {
        continue;
      }

      // if it is a link but there is no corresponding target assembly with the same mode
      std::set< Lattice::CellDrawMode > targetForms;
      for(LabelToModeMap::const_iterator iter2 = cells.begin(); iter2 != cells.end(); ++iter2)
      {
        if (iter2->first.compare(linkTargetAssy->getLabel()) != 0)
        {
          continue;
        }
        targetForms = iter2->second;
      }
      if (targetForms.size() == 0)
      {
        continue;
      }
      // targetForms has the modes of the target
      // check to see if the link mode exists for this target

      // since we have to conditionally check every mode for writing out,
      // we need to create sets containing 1 mode each
      std::set< Lattice::CellDrawMode > forms;
      linkTargetAssy->setPath(coreinfo.dir().absolutePath().toStdString());
      cmbNucAssembly* assemblyClone = correspondingLink->clone();

      for (std::set< Lattice::CellDrawMode >::const_iterator mode = iter->second.begin();
           mode != iter->second.end(); ++mode)
      {
        if (targetForms.count(*mode) == 0)
        {
          forms.insert(*mode);
        }
      }
      this->exportInpFile(assemblyClone, false, forms);
      delete assemblyClone;
    }
  }
  if(this->NuclearCore->changeSinceLastGenerate())
  {
    inpFileWriter::write(coreName.toStdString(), *(this->NuclearCore));
  }

  delete dl;
  delete pl;

  return true;
}

bool cmbNucInpExporter
::exportCylinderINPFile(QString filename, QString random)
{
  std::vector< cmbNucAssembly* > used = this->NuclearCore->GetUsedAssemblies();
  if (used.empty())
  {
    QMessageBox msgBox;
    msgBox.setText("Could not export cylinder");
    msgBox.setInformativeText("In order to generate a outer "
                              "cylinder, the core must use at least"
                              " one assyembly.");
    msgBox.exec();
    return false;
  }
  //clone the pins and ducts.  These are needed for determining layers
  cmbNucPartLibrary * odl = this->NuclearCore->getDuctLibrary();
  cmbNucPartLibrary * pl = this->NuclearCore->getPinLibrary()->clone();
  cmbNucPartLibrary * dl = odl->clone();

  //split ducts if needed
  for(unsigned int i = 0; i < dl->GetNumberOfParts(); ++i)
  {
    DuctCell* dc = dl->GetPart<DuctCell>(i);
    if(!dc->isUsed()) continue;
    dc->splitDucts(this->coreLevelLayers.levels);
  }

  //Generate temp inp file of outer cores of an assembly
  QFileInfo fi(filename);
  cmbNucAssembly * temp = used[0]->clone(pl,dl);
  QString fname = temp->getLabel().toLower() + random + ".inp";
  fname = fname.toLower();
  QString fullPath =fi.dir().absolutePath();
  qDebug() << fullPath << "   " << fname;
  temp->setPath(fullPath.toStdString());
  temp->setFileName(fname.toStdString());

  std::set< Lattice::CellDrawMode > set;
  set.insert(temp->getLattice().getFullCellMode());
  this->exportInpFile(temp, true, set);

  delete temp;
  delete pl;
  delete dl;

  //Generate temp inp file of type geometry of core
  QString corename = QString("core") + random + ".inp";
  fullPath =fi.dir().absoluteFilePath(corename);
  inpFileWriter::writeGSH(fullPath.toStdString(), *NuclearCore, fname.toStdString());
  return true;
}

void cmbNucInpExporter
::updateCoreLayers(bool ignore_regen)
{
  std::vector<double> levels;
  std::set<DuctCell *> dc;
  std::set<PinCell *> pc;
  std::set<double> unique_levels;

  std::vector< cmbNucAssembly* > used = this->NuclearCore->GetUsedAssemblies();
  for(std::vector< cmbNucAssembly* >::const_iterator iter = used.begin();
      iter != used.end(); ++iter)
  {
    cmbNucAssembly * assy = *iter;
    DuctCell * assyDuct = &(assy->getAssyDuct());
    if(assyDuct != NULL) dc.insert(assyDuct);
    std::size_t npc = assy->GetNumberOfPinCells();
    for(std::size_t i = 0; i < npc; ++i)
    {
      pc.insert(assy->GetPinCell(static_cast<int>(i)));
    }
  }
  for(std::set<DuctCell *>::const_iterator iter = dc.begin(); iter != dc.end(); ++iter)
  {
    std::vector<double> tmp = (*iter)->getDuctLayers();
    for(unsigned int i = 0; i < tmp.size(); ++i)
    {
      unique_levels.insert(tmp[i]);
    }
  }
  for(std::set<PinCell *>::const_iterator iter = pc.begin(); iter != pc.end(); ++iter)
  {
    std::vector<double> tmp = (*iter)->getPinLayers();
    for(unsigned int i = 0; i < tmp.size(); ++i)
    {
      unique_levels.insert(tmp[i]);
    }
  }
  for(std::set<double>::const_iterator iter = unique_levels.begin();
      iter != unique_levels.end(); ++iter)
  {
    levels.push_back(*iter);
  }

  std::sort(levels.begin(), levels.end());

  if(!ignore_regen)
  {
    bool same = coreLevelLayers.levels.size() == levels.size();
    for(unsigned int i = 0; i < levels.size() && same; ++i)
    {
      same = levels[i] == coreLevelLayers.levels[i];
    }
    if(!same)
    {
      for(std::vector< cmbNucAssembly* >::const_iterator iter = used.begin();
          iter != used.end(); ++iter)
      {
        (*iter)->setAndTestDiffFromFiles(true);
      }
    }
  }
  coreLevelLayers.levels = levels;
}

bool cmbNucInpExporter
::exportInpFile(cmbNucAssembly * assy, bool isCylinder,
                std::set< Lattice::CellDrawMode > const& forms)
{
  for(std::set< Lattice::CellDrawMode >::const_iterator fiter = forms.begin();
      fiter != forms.end(); ++fiter)
  {
    double deg = assy->getZAxisRotation();
    if( assy->getLattice().getFullCellMode() == Lattice::HEX_FULL_30 )
    {
      deg += 30;
    }
    assy->removeOldTransforms(0);
    if(deg != 0)
    {
      assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, deg));
    }

    Lattice::CellDrawMode mode = *fiter;
    std::string const& fname = assy->getFileName(mode, forms.size());

    switch(mode)
    {
      case Lattice::HEX_SIXTH_FLAT_BOTTOM:
      case Lattice::HEX_SIXTH_VERT_BOTTOM:
      case Lattice::HEX_TWELFTH_BOTTOM:
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::Y, 0, 1));
        break;
      case Lattice::HEX_SIXTH_FLAT_TOP:
      case Lattice::HEX_SIXTH_VERT_TOP:
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, 30));
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::X, 0, 1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, -30));
        break;
      case Lattice::HEX_TWELFTH_TOP:
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, -30));
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::Y, 0, -1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, 30));
        break;
      case Lattice::HEX_SIXTH_FLAT_CENTER:
      case Lattice::HEX_SIXTH_VERT_CENTER:
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::Y, 0, 1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, 30));
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::X, 0, 1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, -30));
        break;
      case Lattice::HEX_TWELFTH_CENTER:
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::Y, 0, 1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, -30));
        assy->addTransform(new cmbNucAssembly::Section( cmbNucAssembly::Transform::Y, 0, -1));
        assy->addTransform(new cmbNucAssembly::Rotate(cmbNucAssembly::Transform::Z, 30));
        break;
      case Lattice::RECT:
      case Lattice::HEX_FULL:
      case Lattice::HEX_FULL_30:
        break;
    }
    QFileInfo assyFile(fname.c_str());

    if(!assyFile.exists() || assy->changeSinceLastGenerate())
    {
      inpFileWriter::write(fname, *assy, this->NuclearCore->getBoundaryLayers(), false, isCylinder);
    }
  }
  assy->removeOldTransforms(0);
  return true;
}
