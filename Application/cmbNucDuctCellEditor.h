#ifndef __cmbNucDuctCellEditor_h
#define __cmbNucDuctCellEditor_h

#include <QWidget>
#include <vtkSmartPointer.h>
#include "vtkMultiBlockDataSet.h"

#include <vector>

#include "ui_cmbDuctCellEditor.h"

#include "cmbNucWidgetChangeCheckingData.h"

class QComboBox;
class DuctCell;
class Duct;
class cmbNucAssembly;
class cmbNucPart;
class cmbNucRender;
class cmbNucCore;
class cmbNucDuctCellEditorInternal;

// the pin cell editor widget. this dialog allows the user to modify a
// single pin cell in an assembly. the sections (cylinders/frustums) can
// be added/removed/modifed as well as the layers. materials can be assigned
// to each layer via the materials table.
class cmbNucDuctCellEditor : public cmbNucCheckableWidget
{
  Q_OBJECT

public:
  cmbNucDuctCellEditor(QWidget *parent = 0);
  ~cmbNucDuctCellEditor();

  void SetDuctCell(DuctCell *pincell, cmbNucCore * core);

  bool isCrossSectioned();

signals:
  void ductcellModified(cmbNucPart*);
  void valueChange();
  void nameChanged(cmbNucPart*, QString prev, QString current);

public slots:
  void update();
  void change();

private slots:
  void ductTableCellSelection();
  void splitDuct();
  void deleteUp();
  void deleteDown();
  void addLayerBefore();
  void addLayerAfter();
  void deleteLayer();
  void onUpdateLayerMaterial();
  void nameChange(QString name);

private:
  void setDuctRow(int i, Duct * d);
  void setDuctMaterialRow(int i, Duct * d);

  void fillMaterialTable(Duct * d);

  void onApply();
  void onReset();
  void onClear()
  {}

  Ui::cmbDuctCellEditor *Ui;
  DuctCell *InternalDuctCell;
  DuctCell *ExternalDuctCell;
  cmbNucAssembly *AssemblyObject;
  bool isHex;

  cmbNucDuctCellEditorInternal * Internal;
};

#endif // __cmbNucDuctCellEditor_h
