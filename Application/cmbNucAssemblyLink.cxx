#include "cmbNucAssemblyLink.h"
#include <QString>

cmbNucAssemblyLink::cmbNucAssemblyLink(cmbNucAssembly *l, int msid, int nsid)
:cmbNucPart("A", "Assembly"), link(l), materialStartID(msid), neumannStartID(nsid)
{
}

cmbNucAssemblyLink::~cmbNucAssemblyLink()
{
  this->deleteConnection(); //removes connections
}
                                                                  

bool cmbNucAssemblyLink::isValid()
{
  return this->link != NULL;
}

cmbNucAssembly * cmbNucAssemblyLink::getLink() const
{
  return this->link;
}

int cmbNucAssemblyLink::getMaterialStartID() const
{
  return this->materialStartID;
}

int cmbNucAssemblyLink::getNeumannStartID() const
{
  return this->neumannStartID;
}

void cmbNucAssemblyLink::setMaterialStartID( int msid )
{
  this->materialStartID = msid;
}

void cmbNucAssemblyLink::setNeumannStartID(int nsid)
{
  this->neumannStartID = nsid;
}

void cmbNucAssemblyLink::setLink(cmbNucAssembly * l)
{
  this->link = l;
}

enumNucPartsType cmbNucAssemblyLink::GetType() const
{
  return CMBNUC_ASSEMBLY_LINK;
}

cmbNucAssembly * cmbNucAssemblyLink::clone()
{
  cmbNucAssembly * result = this->link->clone(this->link->getPinLibrary(),
                                              this->link->getDuctLibrary());
  std::string fname = ("assembly_" + this->getLabel().toLower() + ".inp").toStdString();
  result->setLabel(this->getLabel());
  result->setFileName(fname);
  result->GetParameters()->setMaterialSet_StartId(this->getMaterialStartID());
  result->GetParameters()->setNeumannSet_StartId(this->getNeumannStartID());
  return result;
}
