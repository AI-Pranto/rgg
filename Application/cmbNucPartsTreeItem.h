
#ifndef __cmbNucPartsTreeItem_h
#define __cmbNucPartsTreeItem_h

#include <QTreeWidgetItem>
#include <QFileInfo>
#include <QChar>
#include "cmbNucPart.h"

class  cmbNucPartsTreeItem;

class cmbNucPartsTreeItemConnection: public QObject
{
  friend class cmbNucPartsTreeItem;
  Q_OBJECT
public:
  cmbNucPartsTreeItemConnection()
  {}
  virtual ~cmbNucPartsTreeItemConnection()
  {}
  cmbNucPartsTreeItem * v;
signals:

public slots:
  void checkSaveAndGenerate();
  void objectChanged(cmbNucPart *);
};

class  cmbNucPartsTreeItem : public QTreeWidgetItem
{
  friend class cmbNucPartsTreeItemConnection;
public:
  cmbNucPartsTreeItem(QTreeWidgetItem* pNode, cmbNucPart*, QString label );
  virtual ~cmbNucPartsTreeItem();
  
  // Description:
  // Get the assembly part object  
  virtual cmbNucPart* getPartObject()
    {return this->PartObject;}

  QVariant data( int index, int role ) const;

  cmbNucPartsTreeItemConnection * connection;

protected:
  void setHighlight(int index, bool b, QChar const c[2]);
  void checkSaveAndGenerate();
  enum {NO_TOOL_TIP = 0, NEED_ACTION = 1, OK = 2} toolTipMode[4];
  cmbNucPart* PartObject;
  QFileInfo fileInfo;
};

#endif /* __cmbNucPartsTreeItem_h */
