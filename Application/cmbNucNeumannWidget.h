#ifndef cmbNucNeumannWidget_h
#define cmbNucNeumannWidget_h

#include "cmbNucWidgetChangeCheckingData.h"
#include "cmbNucCore.h"

#include <QString>
#include <QTableWidgetItem>

class cmbNucCore;
class cmbNucNeumannWidgetInternal;

class cmbNucEmmitingTableElement : public QObject, public QTableWidgetItem
{
  Q_OBJECT
public slots:
  void forward(QString str)
  {
    emit cellchanged( this->row(), this->column(), str);
  }
signals:
  void cellchanged(int row, int col, QString);
};

class cmbNucNeumannWidget : public cmbNucCheckableWidget
{
  Q_OBJECT
public:
  cmbNucNeumannWidget(QWidget* p);
  ~cmbNucNeumannWidget();
  void set( cmbNucCore * c );
public slots:
  void checkChange();
protected:
  virtual void onApply();
  virtual void onReset();
  virtual void onClear();
protected slots:
  void onAddToTable();
  void onDeleteRow();
  void addToTable(int row, cmbNucCoreParams::NeumannSetStruct const& n);
  void neumannTypeChanged(int row, int col, QString);
  void setEquationEditable(int row);
private:
  cmbNucNeumannWidgetInternal * Internal;
  cmbNucCore * Core;
};

#endif
