#ifndef cmbNucReactorVesselWidget_h
#define cmbNucReactorVesselWidget_h

#include "cmbNucWidgetChangeChecker.h"
#include "cmbNucCore.h"

#include <QSpinBox>

class cmbNucReactorVesselWidgetInternal;

class cmbNucReactorVesselWidget : public cmbNucCheckableWidget
{
    Q_OBJECT
public:
  cmbNucReactorVesselWidget(QWidget * parent, QSpinBox * spx, QSpinBox * spy );
  ~cmbNucReactorVesselWidget();
  void set(cmbNucCore * core);
public slots:
  void setWidth(double);
  void setHeight(double);
protected:
  virtual void onApply();
  virtual void onReset();
  virtual void onClear();
signals:
  void drawCylinder(double r, int i);
protected slots:
  void checkChange();
  void displayBackgroundControls(int);
  void onCalculateCylinderDefaults(bool checkOld = false);
  void onRadiusChanged(double v);
  void onIntervalChanged(int v);
  void coreSizeChanged(int i);
  void onClearBackgroundMesh();
  void onSetBackgroundMesh();
protected:
  cmbNucReactorVesselWidgetInternal * Internal;
  cmbNucCore * Core;
};

#endif
