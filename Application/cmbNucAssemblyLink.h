#ifndef _cmbNucAssemblyLink_h_
#define _cmbNucAssemblyLink_h_

#include <QObject>
#include <QSet>

#include "cmbNucPart.h"
#include "cmbNucAssembly.h"

class cmbNucAssemblyLink;

class cmbNucAssemblyLink : public cmbNucPart
{

public:
  cmbNucAssemblyLink(cmbNucAssembly *l = NULL, int msid = 0, int nsid = 0);
  virtual ~cmbNucAssemblyLink();
  bool isValid();
  cmbNucAssembly * getLink() const;
  int getMaterialStartID() const;
  int getNeumannStartID() const;
  void setMaterialStartID(int msid );
  void setNeumannStartID(int nsid);
  void setLink(cmbNucAssembly *);

  enumNucPartsType GetType() const;

  virtual QString getTitle()
  {
    return "Assembly: " + cmbNucPart::getName() + " (" + cmbNucPart::getLabel() + ")"
           " --> " + getLink()->getTitle();
  }

  cmbNucAssembly * clone();

protected:
  cmbNucAssembly * link;
  int materialStartID;
  int neumannStartID;
};

#endif
