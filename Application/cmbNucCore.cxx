
#include "cmbNucCore.h"
#include "inpFileIO.h"

#include <iostream>
#include <algorithm>
#include <fstream>
#include <limits>
#include <set>
#include <cmath>

#include "cmbNucPartLibrary.h"
#include "cmbNucAssembly.h"
#include "cmbNucAssemblyLink.h"

#include "vtkTransform.h"
#include "vtkInformation.h"
#include "vtkNew.h"
#include "vtkTransformFilter.h"
#include "vtkMath.h"
#include "vtkPolyData.h"
#include "cmbNucDefaults.h"
#include "cmbNucMaterialColors.h"
#include "vtkBoundingBox.h"
#include "cmbNucPartLibrary.h"
#include "cmbNucCordinateConverter.h"

#include "vtkCmbLayeredConeSource.h"

#include <QFileInfo>
#include <QDateTime>
#include <QDir>
#include <QDebug>

std::string cmbNucCoreParams::getMeshOutputFilename() const
{
  if(meshFilePrefix.empty()) return "";
  return meshFilePrefix + "." + meshFileExtention;
}

void cmbNucCoreParams::setMeshOutputFilename(std::string const& fname)
{
  QFileInfo qf(fname.c_str());
  this->meshFilePrefix = qf.completeBaseName().toStdString();
  this->meshFileExtention = qf.suffix().toStdString();
}

bool cmbNucCoreParams::MeshOutputFilenameIsSet() const
{
  return !meshFilePrefix.empty();
}

void cmbNucCoreParams::clearMeshOutputFilename()
{
  meshFilePrefix = "";
}

cmbNucCore::cmbNucCore(bool needSaved)
:LatticeContainer("Core", "Core", Qt::white, 23.5, 23.5)
{
  this->PinLibrary = new cmbNucPartLibrary;
  this->DuctLibrary = new cmbNucPartLibrary;
  this->HexSymmetry = 1;
  DifferentFromFile = needSaved;
  DifferentFromH5M = true;
  DifferentFromGenBoundaryLayer = true;

  QObject::connect(this->PinLibrary->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                   this->connection,                  SLOT(componentChanged(cmbNucPart*)));
  QObject::connect(this->DuctLibrary->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                   this->connection,                   SLOT(componentChanged(cmbNucPart*)));
}

cmbNucCore::~cmbNucCore()
{
  this->deleteConnection(); //removes connections
  cmbNucPart::deleteObjs(this->parts);

  this->parts.clear();
  this->clearBoundaryLayer();
  delete this->Defaults;
  delete this->PinLibrary;
  delete this->DuctLibrary;
}

vtkBoundingBox cmbNucCore::computeBounds()
{
  DuctCell * cd = this->DuctLibrary->GetPart<DuctCell>(0);
  if(cd == NULL) return vtkBoundingBox();
  double z1, z2;
  cd->getZRange(z1, z2);
  double wh[2];// = {this->getPitchX(), this->getPitchY()};
  this->Defaults->getDuctThickness(wh[0], wh[1]);
  std::pair<int, int> dim = lattice.GetDimensions();
  if(IsHexType())
  {
    double diameter = wh[0]*(dim.first*2-1);
    double radius = diameter*0.5;
    double pointDist = wh[0]*0.5/cmbNucMathConst::cos30;
    double tmpH = (dim.first + std::floor((dim.first-1)*0.5))*pointDist;

    int subType = lattice.GetGeometrySubType();
    double tx = 0, ty = 0;
    double min[2], max[2];
    if((subType & ANGLE_360) && dim.first>=1)
    {
      tx = wh[0]*dim.first;
      double tmp = tx - wh[0];
      double t2 = tmp*0.5;
      ty = -std::sqrt(tmp*tmp-t2*t2);
      double r = std::max(tmpH,radius);
      min[0] = min[1] = -r;
      max[0] = max[1] = r;
    }
    else if((subType & ANGLE_60) && (subType & VERTEX))
    {
      min[0] = 0;
      min[1] = 0;
      max[0] = tmpH;
      max[1] = cmbNucMathConst::cos30*tmpH;
    }
    else if((subType & ANGLE_60))
    {
      min[0] = 0;
      min[1] = 0;
      max[0] = radius;
      max[1] = cmbNucMathConst::cos30*radius;
    }
    else
    {
      min[0] = 0;
      min[1] = 0;
      max[0] = radius;
      max[1] = radius;
    }
    if(getVessel().generate())
    {
      double r = this->getVessel().getCylinderRadius();
      min[0] = std::min(min[0], -r);
      min[1] = std::min(min[1], -r);
      max[0] = std::max(max[0], r);
      max[1] = std::max(max[1], r);
    }
    return vtkBoundingBox(tx+min[0], tx+max[0],
                          ty+min[1], ty+max[1],
                          z1, z2);
  }
  else
  {
    vtkBoundingBox b;
    double transX, transY;
    { //This is normally 0.  we might need to look more into this.
      transX = cd->getDuct(0)->getX();
      transY = cd->getDuct(0)->getY();
    }
    double pt[4];
    calculateRectPt( 0, 0, pt );
    calculateRectPt(dim.first-1, dim.second-1, pt+2);
    pt[0] -= wh[0]*0.5;
    pt[1] -= wh[1]*0.5;
    pt[2] += wh[0]*0.5;
    pt[3] += wh[1]*0.5;
    double cp[] = {(pt[2] + pt[0])*0.5,(pt[3] + pt[1])*0.5};
    double w = pt[2] - pt[0];
    double h = pt[3] - pt[1];
    if(this->getVessel().generate())
    {
      w = std::max(w, 2*this->getVessel().getCylinderRadius());
      h = std::max(h, 2*this->getVessel().getCylinderRadius());
    }
    return vtkBoundingBox(transX+cp[0]-w*0.5, transX+cp[0]+w*0.5,
                          transY+cp[1]-h*0.5, transY+cp[1]+h*0.5,
                          z1, z2);
  }
}

void cmbNucCore::SetDimensions(int i, int j)
{
  this->lattice.SetDimensions(i, j);
}

void cmbNucCore::clearExceptAssembliesAndGeom()
{
  this->lattice.SetDimensions(1, 1, true);
  this->setAndTestDiffFromFiles(true);
  CurrentFileName = "";
  ExportFileName = "";
  Params.clear();
}

std::vector< cmbNucAssembly* > cmbNucCore::GetUsedAssemblies()
{
  std::vector< cmbNucAssembly* > result;
  std::vector< cmbNucPart* > usedParts = this->lattice.getUsedParts();

  for(std::vector<cmbNucPart *>::const_iterator it = usedParts.begin();
      it != usedParts.end(); ++it)
  {
    cmbNucAssembly * a = dynamic_cast<cmbNucAssembly *>(*it);
    if(a != NULL) result.push_back(a);
  }
  return result;
}

std::vector< cmbNucAssemblyLink* > cmbNucCore::GetUsedLinks()
{
  std::vector< cmbNucAssemblyLink* > result;
  std::vector< cmbNucPart* > usedParts = this->lattice.getUsedParts();

  for(std::vector<cmbNucPart *>::const_iterator it = usedParts.begin();
      it != usedParts.end(); ++it)
  {
    cmbNucAssemblyLink * a = dynamic_cast<cmbNucAssemblyLink *>(*it);
    if(a != NULL) result.push_back(a);
  }
  return result;
}

std::vector< cmbNucPart* > cmbNucCore::GetUsedBlanks()
{
  std::vector< cmbNucPart* > result;
  std::vector< cmbNucPart* > usedParts = this->lattice.getUsedParts();

  for(std::vector<cmbNucPart *>::const_iterator it = usedParts.begin();
      it != usedParts.end(); ++it)
  {
    if((*it)->GetType() == CMBNUC_ASSY_BASEOBJ) result.push_back(*it);
  }
  return result;
}

void cmbNucCore::calculateRectPt(unsigned int i, unsigned int j, double pt[2])
{
  double outerDuctWidth = this->getPitchX();
  double outerDuctHeight = this->getPitchY();
  pt[1] = i * (outerDuctHeight)-outerDuctHeight*(this->lattice.getSize()-1);
  pt[0] = j * (outerDuctWidth);
}

void cmbNucCore::calculateExtraTranslation(double & transX, double & transY)
{
  transX = 0; transY = 0;
  if(this->IsHexType())
  {
    int subType = this->getLattice().GetGeometrySubType();

    if( (subType & ANGLE_360) && this->getLattice().getSize()>=1 )
    {
      double outerDuctHeight = this->getPitchY();
      //Defaults->getDuctThickness(outerDuctWidth, outerDuctHeight);
      double odc = outerDuctHeight * this->getLattice().getSize();
      double tmp = odc - outerDuctHeight;
      double t2 = tmp*0.5;
      double ty = std::sqrt(tmp*tmp-t2*t2);
      transY = -ty;
      transX = odc;
    }
  }
  else
  {
    DuctCell * cd = this->DuctLibrary->GetPart<DuctCell>(0);
    transX = cd->getDuct(0)->getX();
    transY = cd->getDuct(0)->getY();
  }
}

void cmbNucCore::calculateTranslation(double & transX, double & transY)
{
  DuctCell * cd = this->DuctLibrary->GetPart<DuctCell>(0);
  transX = cd->getDuct(0)->getX();
  transY = cd->getDuct(0)->getY();
  double outerDuctHeight = this->getPitchY();
  //Defaults->getDuctThickness(outerDuctWidth, outerDuctHeight);
  if(this->IsHexType())
  {
    int subType = this->getLattice().GetGeometrySubType();

    if( (subType & ANGLE_360) && this->getLattice().getSize()>=1 )
    {
      double odc = outerDuctHeight * this->getLattice().getSize();
      double tmp = odc - outerDuctHeight;
      double t2 = tmp*0.5;
      double ty = std::sqrt(tmp*tmp-t2*t2);
      transY += -ty;
      transX += odc;
    }
  }
  else
  {
    transY -= outerDuctHeight*(this->lattice.getSize()-1);
  }
}

void cmbNucCore::setGeometryLabel(std::string geomType)
{
  int subType = lattice.GetGeometrySubType() & JUST_ANGLE;
  std::transform(geomType.begin(), geomType.end(), geomType.begin(), ::tolower);
  enumGeometryType type = HEXAGONAL;
  if( geomType == "hexflat" )
  {
    subType |= FLAT;
  }
  else if(geomType == "hexvertex")
  {
    subType |= VERTEX;
  }
  else
  {
    type = RECTILINEAR;
    subType = ANGLE_360;
  }
  lattice.SetGeometryType(type);
  lattice.SetGeometrySubType(subType);
}

void cmbNucCore::setHexSymmetry(int sym)
{
  int type = lattice.GetGeometrySubType() & ~JUST_ANGLE; // clear angle
  if(sym == 6)
  {
    type |= ANGLE_60;
  }
  else if(sym == 12)
  {
    type |= ANGLE_30;
  }
  else //all others treated as 360
  {
    type |= ANGLE_360;
  }
  lattice.SetGeometrySubType(type);
}

bool cmbNucCore::IsHexType()
{
  return HEXAGONAL == lattice.GetGeometryType();
}

void cmbNucCore::SetLegendColors(int numDefaultColors, int defaultColors[][3])
{
  unsigned int cI = 0;
  for(unsigned int i = 0; i < this->parts.size(); ++i)
  {
    cmbNucPart * part = this->parts[i];
    int aci = cI++  % numDefaultColors;
    QColor acolor(defaultColors[aci][0], defaultColors[aci][1], defaultColors[aci][2]);
    part->SetLegendColor(acolor);
  }
}

void cmbNucCore::computePitch()
{
  std::vector< cmbNucAssembly* > assemblies = this->GetUsedAssemblies();
  double px = 0;
  double py = 0;
  double tmp[2];
  for (unsigned int i = 0; i < assemblies.size(); ++i)
  {
    if(assemblies[i]==NULL) continue;
    assemblies[i]->GetDuctWidthHeight(tmp);
    if(tmp[0]>px) px = tmp[0];
    if(tmp[1]>py) py = tmp[1];
  }
  this->setPitch(px, py);
}

void cmbNucCore::fileChanged()
{
  bool previous_file_state = this->changeSinceLastGenerate();
  this->DifferentFromFile = true;
  if(previous_file_state!=this->changeSinceLastGenerate()) this->checkChangeState();
}

void cmbNucCore::boundaryLayerChanged()
{
  bool previous_file_state = this->changeSinceLastGenerate();
  std::vector< cmbNucAssembly* > assy = this->GetUsedAssemblies();
  for(size_t i = 0; i < assy.size(); ++i )
  {
    //TODO Fix this
    //assy[i]->GetConnection()->geometryChanged();
  }
  this->DifferentFromGenBoundaryLayer = true;
  this->DifferentFromFile = true;
  if(previous_file_state!=this->changeSinceLastGenerate()) this->checkChangeState();
}

void cmbNucCore::setAndTestDiffFromFiles(bool diffFromFile)
{
  bool previous_file_state = this->changeSinceLastGenerate();

  if(diffFromFile)
  {
    this->fileChanged();
    this->DifferentFromH5M = true;
    this->DifferentFromGenBoundaryLayer = true;
    return;
  }
  //make sure file exits
  //check to see if a h5m file has been generate and is older than this file
  QFileInfo inpInfo(this->CurrentFileName.c_str());
  if(!inpInfo.exists())
  {
    this->fileChanged();
    this->DifferentFromH5M = true;
    this->DifferentFromGenBoundaryLayer = true;
    return;
  }
  this->DifferentFromFile = false;
  if(this->ExportFileName.empty())
  {
    this->DifferentFromH5M = true;
    this->DifferentFromGenBoundaryLayer = true;
    return;
  }
  //QFileInfo h5mFI();
  QDateTime inpLM = inpInfo.lastModified();
  QFileInfo exportInfo(this->ExportFileName.c_str());
  QFileInfo h5mInfo(exportInfo.dir(), this->getMeshOutputFilename().c_str());
  if(!h5mInfo.exists())
  {
    this->DifferentFromH5M = true;
    return;
  }
  QDateTime h5mLM = h5mInfo.lastModified();
  this->DifferentFromH5M = h5mLM < inpLM;
  if(getNumberOfBoundaryLayers() != 0)
  {
    h5mInfo = QFileInfo(exportInfo.dir(), this->getMeshOutputFilename().c_str());
    if(!h5mInfo.exists())
    {
      this->DifferentFromGenBoundaryLayer = true;
      return;
    }
    h5mLM = h5mInfo.lastModified();
    this->DifferentFromGenBoundaryLayer = h5mLM < inpLM;
  }
  else
  {
    this->DifferentFromGenBoundaryLayer = this->DifferentFromH5M;
  }
  this->checkUsedAssembliesForGen_imple();
  if(previous_file_state!=this->changeSinceLastGenerate()) this->checkChangeState();
}

void cmbNucCore::checkUsedAssembliesForGen()
{
  bool previous_file_state = this->changeSinceLastGenerate();
  checkUsedAssembliesForGen_imple();
  if(previous_file_state!=this->changeSinceLastGenerate()) this->checkChangeState();
}

void cmbNucCore::checkUsedAssembliesForGen_imple()
{
  if(this->DifferentFromH5M) return;
  QFileInfo corefi(this->ExportFileName.c_str());
  QFileInfo h5mInfo(corefi.dir(),
                    this->getMeshOutputFilename().c_str());
  std::vector< cmbNucAssembly* > assy = this->GetUsedAssemblies();
  for(unsigned int i = 0; i < assy.size() && !this->DifferentFromH5M; ++i)
  {
    this->DifferentFromH5M |= assy[i]->changeSinceLastGenerate();
    QFileInfo inpInfo(assy[i]->getFileName().c_str());
    QFileInfo cubInfo(corefi.dir(), inpInfo.baseName() + ".cub");
    this->DifferentFromH5M |= !cubInfo.exists() || h5mInfo.lastModified() < cubInfo.lastModified();
  }
}

bool cmbNucCore::changeSinceLastSave() const
{
  return this->DifferentFromFile;
}

bool cmbNucCore::changeSinceLastGenerate() const
{
  return this->DifferentFromH5M;
}

QPointer<cmbNucDefaults> cmbNucCore::GetDefaults()
{
  return this->Defaults;
}

bool cmbNucCore::HasDefaults() const
{
  return this->Defaults != NULL;
}

void cmbNucCore::initDefaults()
{
  double DuctThickX = 10;
  double DuctThickY = 10;
  double length = 10;
  double z0 = 0;
  delete this->Defaults;
  this->Defaults = new cmbNucDefaults();
  this->Defaults->setHeight(length);
  this->Defaults->setZ0(z0);

  this->Defaults->setDuctThickness(DuctThickX, DuctThickY);
  this->setPitch(DuctThickX, DuctThickY);
}

void cmbNucCore::calculateDefaults()
{
  delete this->Defaults;
  this->Defaults = new cmbNucDefaults();
  std::vector< cmbNucAssembly* > assys = this->GetUsedAssemblies();

  double AxialMeshSize = 1e23;
  int    EdgeInterval = 2147483647;

  double DuctThickX = -1;
  double DuctThickY = -1;
  double length = -1;
  double z0 = -1e23;
  QString MeshType;

  for(unsigned int i = 0; i < assys.size(); ++i)
  {
    cmbNucAssembly * assy = assys[i];
    DuctCell & dc = assy->getAssyDuct();
    cmbAssyParameters* params =  assy->GetParameters();
    if(params->isSetAxialMeshSize() &&
       params->getAxialMeshSize() < AxialMeshSize)
      AxialMeshSize = params->getAxialMeshSize();
    if(params->isSetEdgeInterval() &&
       params->getEdgeInterval() < EdgeInterval)
      EdgeInterval = params->getEdgeInterval();
    if(MeshType.isEmpty() && params->isSetMeshType())
    {
      MeshType = params->getMeshType().c_str();
    }
    if(length < dc.getLength()) length = dc.getLength();
    double z[2];
    dc.getZRange(z[0], z[1]);
    if(z0 < z[0]) z0 = z[0];

    double r[2];
    assy->GetDuctWidthHeight(r);
    if( r[0] > DuctThickX ) DuctThickX = r[0];
    if( r[1] > DuctThickY ) DuctThickY = r[1];
  }
  if(AxialMeshSize != 1e23)
    this->Defaults->setAxialMeshSize(AxialMeshSize);
  if(EdgeInterval != 2147483647)
    this->Defaults->setEdgeInterval(EdgeInterval);
  if(!MeshType.isEmpty()) this->Defaults->setMeshType(MeshType);
  if(length != -1) this->Defaults->setHeight(length);
  if(z0 != -1e23) this->Defaults->setZ0(z0);

  this->Defaults->setDuctThickness(DuctThickX, DuctThickY);
  this->sendDefaults();
}

void cmbNucCore::sendDefaults()
{
  for(unsigned int i = 0; i < parts.size(); ++i)
  {
    cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly*>(parts[i]);
    if(assy) assy->setFromDefaults(this->Defaults);
  }

  double dt1, dt2, l, z0;
  this->Defaults->getDuctThickness(dt1,dt2);
  this->setPitch(dt1, dt2);
  l = this->Defaults->getHeight();
  z0 = this->Defaults->getZ0();

  for(size_t i = 0; i < this->DuctLibrary->GetNumberOfParts(); ++i)
  {
    DuctCell * dc = this->DuctLibrary->GetPart<DuctCell>(static_cast<int>(i));
    dc->setDuctThickness(dt1,dt2);
    dc->setLength(l);
    dc->setZ0(z0);
  }
}

bool cmbNucCore::name_unique(QString const& in)
{
  QString n = in.toLower();
  for (size_t i=0; i < this->parts.size(); ++i)
  {
    if (this->parts[i]->getName().toLower() == n) return false;
  }
  return true;
}

bool cmbNucCore::label_unique(QString const& in)
{
  QString n = in.toLower();
  for(size_t i = 0; i < this->parts.size(); ++i)
  {
    if(this->parts[i]->getLabel().toLower() == n) return false;
  }
  return true;
}

void cmbNucCore::fillList(std::vector< std::pair<QString, cmbNucPart *> > & l)
{
  l.push_back(std::pair<QString, cmbNucPart *>("XX", NULL));
  for(size_t i = 0; i < this->getNumberOfParts(); i++)
  {
    l.push_back(std::pair<QString, cmbNucPart *>(this->getPart(i)->getLabel(), this->getPart(i)));
  }
}

bool cmbNucCore::addPart(cmbNucPart * part)
{
  if(part == NULL) return false;
  if( part->GetType() == CMBNUC_ASSEMBLY )
  {
    cmbNucAssembly * assembly = dynamic_cast<cmbNucAssembly*>(part);
    if(this->parts.size()==0)
    {
      this->lattice.SetGeometryType(assembly->getLattice().GetGeometryType());
      this->SetDimensions(1, 1);
      this->lattice.SetCell(0, 0, part);
    }
    assembly->setPinLibrary(this->PinLibrary);
    assembly->setDuctLibrary(this->DuctLibrary);
    if( this->getLattice().GetGeometrySubType() & ANGLE_60 &&
        this->getLattice().GetGeometrySubType() & VERTEX ) //TODO reorganize this to remove if
    {
      assembly->getLattice().setFullCellMode(Lattice::HEX_FULL);
    }
    else
    {
      assembly->getLattice().setFullCellMode(Lattice::HEX_FULL_30);
    }
  }
  else if(part->GetType() == CMBNUC_ASSEMBLY_LINK)
  {
    cmbNucAssemblyLink * assyLink = dynamic_cast<cmbNucAssemblyLink*>(part);
    if(!assyLink->isValid()) return false;
    cmbNucAssembly *a1 = assyLink->getLink();
    cmbNucAssembly *a2 = dynamic_cast<cmbNucAssembly*>(this->getFromLabel(a1->getLabel()));
    if(a2 == NULL) return false;
    if(a2 != a1) assyLink->setLink(a2);
  }
  this->parts.push_back(part);
  this->connectObj(part);
  return true;
}

cmbNucPart * cmbNucCore::getPart(size_t i)
{
  if(i >= this->parts.size()) return NULL;
  return this->parts[i];
}

size_t cmbNucCore::getNumberOfParts()
{
  return this->parts.size();
}

cmbNucPart * cmbNucCore::getFromLabel(const QString & s)
{
  for(size_t i = 0; i < this->parts.size(); i++)
  {
    if(this->parts[i]->getLabel() == s)
    {
      return this->parts[i];
    }
  }
  return NULL;
}

void cmbNucCore::drawCylinder(double r, int i)
{
  if(r <= 0 || i <= 0)
  {
    return;
  }
  this->getVessel().setCylinderRadius(r);
  this->getVessel().setCylinderOuterSpacing(i);
}

void cmbNucCore::clearCylinder()
{
}

std::map< QString, std::set< Lattice::CellDrawMode> > cmbNucCore::getDrawModesForAssemblies()
{
  std::map< QString, std::set< Lattice::CellDrawMode> > result;
  for(size_t i = 0; i < lattice.getSize(); i++) //layer
  {
    const int ti = static_cast<int>(i);
    for(size_t j = 0; j < lattice.getSize(i); j++) //index on ring
    {
      const int tj = static_cast<int>(j);
      if(!lattice.GetCell(ti, tj).isBlank())
      {
        result[lattice.GetCell(ti, tj).getLabel()].insert(lattice.getDrawMode(/*index*/ tj,
                                                                              /*Layer*/ ti) );
      }
    }
  }
  return result;
}

void cmbNucCore::addBoundaryLayer(boundaryLayer * bl)
{
  if(bl == NULL) return;
  this->BoundaryLayers.push_back(bl);
}

int cmbNucCore::getNumberOfBoundaryLayers() const
{
  return static_cast<int>(this->BoundaryLayers.size());
}

void cmbNucCore::removeBoundaryLayer(size_t bl)
{
  if(static_cast<size_t>(bl)>= this->BoundaryLayers.size())
  {
    return;
  }
  this->BoundaryLayers.erase(this->BoundaryLayers.begin() + bl);
}

void cmbNucCore::clearBoundaryLayer()
{
  for(size_t i = 0; i < this->BoundaryLayers.size(); ++i)
  {
    delete this->BoundaryLayers[i];
  }
  BoundaryLayers.clear();
}

cmbNucCore::boundaryLayer * cmbNucCore::getBoundaryLayer(int bl) const
{
  if(static_cast<size_t>(bl)>= this->BoundaryLayers.size())
  {
    return NULL;
  }
  return BoundaryLayers[bl];
}

std::string cmbNucCore::getMeshOutputFilename() const
{
  return this->Params.getMeshOutputFilename();
}

void cmbNucCore::setMeshOutputFilename(std::string const& fname)
{
  this->Params.setMeshOutputFilename(fname);
}

std::string const& cmbNucCore::getExportFileName() const
{
  return this->ExportFileName;
}

void cmbNucCore::setExportFileName(std::string const& fname)
{
  this->ExportFileName = fname;
  if(!this->Params.MeshOutputFilenameIsSet())
  {
    QFileInfo qf(fname.c_str());
    this->setMeshOutputFilename(qf.completeBaseName().toStdString() + "." + "h5m");
  }
}

void cmbNucCore::setFileName( std::string const& fname )
{
  this->CurrentFileName = fname;
}

/// Ask for a Id to use for Neumann or Material IDs
int cmbNucCore::getFreeId()
{
  std::set<int> occupiedIds = getOccupiedIds(NULL);

  int potentialId = 100; // min value for auto-generated IDs
  while (potentialId < INT_MAX-100)
  {
    if (occupiedIds.find(potentialId) != occupiedIds.end())
    {
      // id found, go to next
      potentialId += 100;
    }
    else
    {
      // this Id is okay to use
      break;
    }
  }

  return potentialId;
}

// Same as getFreeId(), but in increments of 1000
// Used for AssemblyLinks
int cmbNucCore::getFreeLinkId(int parentId)
{
  std::set<int> occupiedIds = getOccupiedIds(NULL);

  int potentialId = parentId + 1000; // same-as is strictly larger than parent
  while (potentialId < INT_MAX-1000)
  {
    if (occupiedIds.find(potentialId) != occupiedIds.end())
    {
      // id found, go to next
      potentialId += 1000;
    }
    else
    {
      // this Id is okay to use
      break;
    }
  }

  return potentialId;
}

/// Returns True if id is free (can be used for nid/mid)
bool cmbNucCore::isFreeId(cmbNucPart* exclude, int id)
{
  std::set<int> occupiedIds = getOccupiedIds(exclude);

  int rounded = id - (id%100);

  return (occupiedIds.find(rounded) == occupiedIds.end());
}

/// Return a vector of ints that are multiples of 100
/// They represent all Neumann or Material Ids that are
/// being used by any assembly
std::set<int> cmbNucCore::getOccupiedIds(cmbNucPart *exclude)
{
  std::set<int> ret;

  for (std::vector<cmbNucPart*>::const_iterator iter = parts.begin(); iter != parts.end(); ++iter)
  {
    if (*iter == exclude)
        continue;
    int nid, mid;
    cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly*>(*iter);
    cmbNucAssemblyLink * link = dynamic_cast<cmbNucAssemblyLink*>(*iter);
    if(assy != NULL)
    {
      nid = assy->GetParameters()->getNeumannSet_StartId();
      mid = assy->GetParameters()->getMaterialSet_StartId();
    }
    else if( link != NULL)
    {
      nid = link->getNeumannStartID();
      mid = link->getMaterialStartID();
    }
    else
    {
      continue;
    }

    // round down to nearest 100
    nid -= nid%100;
    mid -= mid%100;

    // since we are working with a set no duplicates will be added
    ret.insert(nid);
    ret.insert(mid);
  }
  return ret;
}

void cmbNucCore::componentPartDeleted(cmbNucPart * obj)
{
  bool changed = false;
  switch( obj->GetType() )
  {
    case CMBNUC_ASSEMBLY:
    {
      cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly*>(obj);
      //remove links
      for(size_t i = 0; i < this->parts.size();)
      {
        cmbNucAssemblyLink * link = dynamic_cast<cmbNucAssemblyLink*>(this->parts[i]);
        if(link != NULL && link->getLink() == assy)
        {
          delete link;
        }
        else
        {
          ++i;
        }
      }
    }
    case CMBNUC_ASSEMBLY_LINK:
    case CMBNUC_ASSY_BASEOBJ:
      changed = cmbNucPart::removeObj(obj, this->parts, false /*do not delete*/);
      break;
    default:
      break;
  }
  if(changed)
  {
    this->fileChanged();
    this->sendChanged();
    // update the Grid
    if(this->lattice.deletePart(obj))
    {
      this->setAndTestDiffFromFiles(true);
    }
  }
}

void cmbNucCore::componentPartChanged(cmbNucPart * obj)
{
  if(obj == NULL || obj->GetType() != CMBNUC_ASSEMBLY)
  {
    this->fileChanged();
  }
  else
  {
    setAndTestDiffFromFiles(true);
  }
  this->sendChanged();
}
