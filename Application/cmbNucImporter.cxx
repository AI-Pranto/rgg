#include "cmbNucImporter.h"
#include "cmbNucCore.h"
#include "inpFileIO.h"
#include "cmbNucMainWindow.h"
#include "cmbNucInputListWidget.h"
#include "cmbNucAssembly.h"
#include "cmbNucInputPropertiesWidget.h"
#include "xmlFileIO.h"
#include "cmbNucDefaults.h"
#include "cmbNucMaterialColors.h"
#include "cmbNucPartLibrary.h"

#include <set>

#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QDebug>

extern int defaultAssemblyColors[][3];
extern int numAssemblyDefaultColors;

cmbNucImporter::cmbNucImporter(cmbNucMainWindow * mw)
:mainWindow(mw)
{
}

bool cmbNucImporter::importXMLPins()
{
  QStringList fileNames = this->getXMLFiles();
  if(fileNames.count()==0)
  {
    return false;
  }

  std::map<QString, QString> junk;

  assert(mainWindow->NuclearCore->HasDefaults());
  double tmpD = mainWindow->NuclearCore->GetDefaults()->getHeight();

  cmbNucMaterialColors * materials = new cmbNucMaterialColors;

  for( int i = 0; i < fileNames.count(); ++i)
  {
    materials->clear();

    std::vector<PinCell*> pincells;

    if(!xmlFileReader::read(fileNames[i].toStdString(), pincells, materials)) return false;
    for(unsigned int j = 0; j < pincells.size(); ++j)
    {
      PinCell* added = this->addPin(pincells[j], tmpD, junk, true);
      assert(added == pincells[j]);
    }
  }
  delete materials;
  return true;
}

bool cmbNucImporter::importXMLDucts()
{
  QStringList fileNames = this->getXMLFiles();
  if(fileNames.count()==0)
  {
    return false;
  }

  std::map<QString, QString> junk;

  assert(mainWindow->NuclearCore->HasDefaults());
  double tmpD = mainWindow->NuclearCore->GetDefaults()->getHeight();

  double ductThick[] = {10.0, 10.0};
  mainWindow->NuclearCore->GetDefaults()->getDuctThickness(ductThick[0],ductThick[1]);

  cmbNucMaterialColors * materials = new cmbNucMaterialColors;

  for( int i = 0; i < fileNames.count(); ++i)
  {
    materials->clear();

    std::vector<DuctCell*> ductcells;

    if(!xmlFileReader::read(fileNames[i].toStdString(), ductcells, materials)) return false;
    for(unsigned int j = 0; j < ductcells.size(); ++j)
    {
      this->addDuct(ductcells[j], tmpD, ductThick, junk, true);
    }
  }
  delete materials;
  return true;
}

bool cmbNucImporter::importInpFile()
{
  // Use cached value for last used directory if there is one,
  // or default to the user's home dir if not.
  QSettings settings("CMBNuclear", "CMBNuclear");
  QDir dir = settings.value("cache/lastDir", QDir::homePath()).toString();

  QStringList fileNames =
  QFileDialog::getOpenFileNames(mainWindow, "Open File...", dir.path(), "INP Files (*.inp)");
  if(fileNames.count()==0)
  {
    return false;
  }

  // Cache the directory for the next time the dialog is opened
  QFileInfo info(fileNames[0]);
  settings.setValue("cache/lastDir", info.dir().path());
  int numExistingAssy = mainWindow->NuclearCore->getNumberOfParts();

  for( int i = 0; i < fileNames.count(); ++i)
  {
    inpFileReader freader;
    switch(freader.open(fileNames[i].toStdString()))
    {
      case inpFileReader::ASSEMBLY_TYPE:
      {
        if(!mainWindow->InputsWidget->isEnabled() || mainWindow->InputsWidget->onlyMeshLoaded())
        {
          mainWindow->doClearAll(true);
        }
        QFileInfo finfo(fileNames[i]);
        QString label = finfo.completeBaseName();

        QString name = finfo.baseName();

        int acolorIndex = numExistingAssy +
                        mainWindow->NuclearCore->getNumberOfParts() % numAssemblyDefaultColors;

        QColor acolor(defaultAssemblyColors[acolorIndex][0],
                      defaultAssemblyColors[acolorIndex][1],
                      defaultAssemblyColors[acolorIndex][2]);

        cmbNucAssembly *assembly = new cmbNucAssembly(name, label, acolor);
        if(!freader.read(*assembly, mainWindow->NuclearCore->getPinLibrary(),
                         mainWindow->NuclearCore->getDuctLibrary()))
        {
          QMessageBox msgBox;
          msgBox.setText("Invalid INP file");
          msgBox.setInformativeText(fileNames[i]+" could not be readed.");
          msgBox.exec();
          delete assembly;
          return false;
        }
        if(mainWindow->InputsWidget->isEnabled() &&
           assembly->IsHexType() != mainWindow->NuclearCore->IsHexType())
        {
          QMessageBox msgBox;
          msgBox.setText("Not the same type");
          msgBox.setInformativeText(fileNames[i]+" is not the same geometry type as current core.");
          msgBox.exec();
          delete assembly;
          return false;
        }
        bool need_to_calc_defaults = mainWindow->NuclearCore->getNumberOfParts() == 0;

        boost::function<bool (QString)> fun =
                              boost::bind(&cmbNucCore::label_unique, mainWindow->NuclearCore, _1);
        assembly->setLabel(cmbNucPart::getUnique(fun, assembly->getLabel(), false, false));
        fun = boost::bind(&cmbNucCore::name_unique, mainWindow->NuclearCore, _1);
        assembly->setName(cmbNucPart::getUnique(fun, assembly->getName(), false, false));

        mainWindow->NuclearCore->addPart(assembly);
        if(need_to_calc_defaults)
        {
          mainWindow->NuclearCore->calculateDefaults();
          switch(assembly->getLattice().GetGeometryType())
          {
            case RECTILINEAR:
              mainWindow->NuclearCore->setGeometryLabel("Rectangular");
              break;
            case HEXAGONAL:
              mainWindow->NuclearCore->setGeometryLabel("HexFlat");
              break;
          }
        }
        else
        {
          assembly->setFromDefaults( mainWindow->NuclearCore->GetDefaults() );
        }

        if( mainWindow->NuclearCore->getLattice().GetGeometrySubType() & ANGLE_60 &&
           mainWindow->NuclearCore->getLattice().GetGeometrySubType() & VERTEX )
        {
          mainWindow->NuclearCore->getLattice().setFullCellMode(Lattice::HEX_FULL);
          assembly->getLattice().setFullCellMode(Lattice::HEX_FULL);
        }
        else
        {
          mainWindow->NuclearCore->getLattice().setFullCellMode(Lattice::HEX_FULL);
          assembly->getLattice().setFullCellMode(Lattice::HEX_FULL_30);
        }
        assembly->adjustRotation();
        break;
      }
      case inpFileReader::CORE_TYPE:
        // clear old assembly
        if(!mainWindow->checkFilesBeforePreceeding()) return false;
        mainWindow->doClearAll();
        log.clear();
        mainWindow->PropertyWidget->setObject(NULL, NULL);
        mainWindow->PropertyWidget->setAssembly(NULL);
        if(!freader.read(*(mainWindow->NuclearCore)))
        {
          QMessageBox msgBox;
          msgBox.setText("Invalid INP file");
          msgBox.setInformativeText(fileNames[i]+" could not be readed.");
          msgBox.exec();
          mainWindow->unsetCursor();
          return false;
        }
        mainWindow->NuclearCore->setAndTestDiffFromFiles(false);
        mainWindow->NuclearCore->SetLegendColors(numAssemblyDefaultColors, defaultAssemblyColors);
        mainWindow->NuclearCore->getLattice().setFullCellMode(Lattice::HEX_FULL);

        for(size_t j = 0; j < mainWindow->NuclearCore->getNumberOfParts(); ++j)
        {
          cmbNucAssembly * assy =
                                dynamic_cast<cmbNucAssembly*>(mainWindow->NuclearCore->getPart(j));
          if(assy == NULL)
            continue;
          assy->adjustRotation();
          if( mainWindow->NuclearCore->getLattice().GetGeometrySubType() & ANGLE_60 &&
             mainWindow->NuclearCore->getLattice().GetGeometrySubType() & VERTEX )
          {
            assy->getLattice().setFullCellMode(Lattice::HEX_FULL);
          }
          else
          {
            assy->getLattice().setFullCellMode(Lattice::HEX_FULL_30);
          }
        }
        break;
      default:
        qDebug() << "could not open" << fileNames[i];
        QMessageBox msgBox;
        msgBox.setText("Could Not Open File");
        msgBox.setInformativeText(fileNames[i]+" is not a valid core or assembly file.");
        msgBox.exec();
        return false;
    }
    std::vector<std::string> tlog = freader.getLog();
    log.insert(log.end(), tlog.begin(), tlog.end());
  }
  int numNewAssy = mainWindow->NuclearCore->getNumberOfParts() - numExistingAssy;

  // update data colors
  mainWindow->updateCoreMaterialColors();

  // In case the loaded core adds new materials
  // TODO Fix this
  mainWindow->InputsWidget->updateUI(numNewAssy);

  return true;
}

bool cmbNucImporter::importXMLAssembly(bool keepDuplicatePins, bool keepDuplicateDucts)
{
  QStringList fileNames = this->getXMLFiles();
  if(fileNames.count()==0)
  {
    return false;
  }

  cmbNucPartLibrary * dl = new cmbNucPartLibrary();
  cmbNucPartLibrary * pl = new cmbNucPartLibrary();
  cmbNucMaterialColors * materials = new cmbNucMaterialColors;

  assert(mainWindow->NuclearCore->HasDefaults());
  double tmpD = mainWindow->NuclearCore->GetDefaults()->getHeight();

  double ductThick[] = {10.0, 10.0};
  mainWindow->NuclearCore->GetDefaults()->getDuctThickness(ductThick[0],ductThick[1]);

  for( int i = 0; i < fileNames.count(); ++i)
  {
    materials->clear();

    std::vector<cmbNucAssembly *> assys;
    std::map<PinCell*, PinCell*> addedPin;
    std::map<DuctCell*, DuctCell*> addedDuct;
    std::map<QString, QString> ductMap;
    std::map<QString, QString> pinMap;

    xmlFileReader::read(fileNames[i].toStdString(), assys, pl, dl, materials);
    for(size_t j = 0; j < assys.size(); ++j)
    {
      cmbNucAssembly * assy = assys[j];
      if(assy->IsHexType() != mainWindow->NuclearCore->IsHexType())
      {
        QMessageBox msgBox;
        msgBox.setText("Not the same type");
        msgBox.setInformativeText(fileNames[i]+" is not the same geometry type as current core.");
        msgBox.exec();
        for(size_t k = 0; k < assys.size(); ++k)
        {
          delete assys[k];
        }
        j = assys.size();
        continue;
      }
      //update duct
      DuctCell * aduct = &(assy->getAssyDuct());
      if(addedDuct.find(aduct) != addedDuct.end())
      {
        assy->setDuctCell(addedDuct[aduct]);
      }
      else
      {
        DuctCell * dc = new DuctCell();
        dc->fill(aduct);
        dc = this->addDuct(dc, tmpD, ductThick, ductMap, !keepDuplicateDucts);
        addedDuct[aduct] = dc;
        assy->setDuctCell(dc);
      }
      std::map<QString, QString> jnk;

      //update pins
      for(std::size_t k = 0; k < assy->GetNumberOfPinCells(); ++k)
      {
        PinCell* old = assy->GetPinCell(static_cast<int>(k));
        PinCell* newpc = NULL;
        if(addedPin.find(old) != addedPin.end())
        {
          newpc = addedPin[old];
        }
        else
        {
          newpc = new PinCell;
          newpc->fill(old);
          newpc->SetLegendColor(old->GetLegendColor());
          newpc = this->addPin(newpc, tmpD, jnk, !keepDuplicatePins);
          addedPin[old] = newpc;
        }
        assy->SetPinCell(static_cast<int>(k), newpc);
        assy->getLattice().replacePart(old, newpc);
      }

      //add assy
      boost::function<bool (QString)> fun =
                                boost::bind(&cmbNucCore::label_unique, mainWindow->NuclearCore, _1);
      assy->setLabel(cmbNucPart::getUnique(fun, assy->getLabel(), false, false));
      fun = boost::bind(&cmbNucCore::name_unique, mainWindow->NuclearCore, _1);
      assy->setName(cmbNucPart::getUnique(fun, assy->getName(), false, false));
      mainWindow->NuclearCore->addPart(assy);
      mainWindow->NuclearCore->fileChanged();
    }
  }
  delete materials;
  delete dl;
  delete pl;
  return true;
}

PinCell * cmbNucImporter::addPin(PinCell * pc, double dh, std::map<QString, QString> & nc,
                                 bool addDuplicate)
{
  cmbNucPartLibrary * pinLib = mainWindow->NuclearCore->getPinLibrary();
  pc->setHeight(dh);
  //adjust materials
  if(pc->cellMaterialSet())
  {
    QPointer<cmbNucMaterial> cm = pc->getCellMaterial();
    QPointer<cmbNucMaterial> tmpm = this->getMaterial(pc->getCellMaterial());
    assert(tmpm != NULL);
    assert(tmpm != cmbNucMaterialColors::instance()->getUnknownMaterial());
    pc->setCellMaterial(tmpm);
  }
  int layers = pc->GetNumberOfLayers();
  for(int k = 0; k < layers; ++k)
  {
    QPointer<cmbNucMaterial> tmpm = this->getMaterial(pc->Material(k));
    assert(tmpm != NULL);
    assert(tmpm != cmbNucMaterialColors::instance()->getUnknownMaterial());
    pc->SetMaterial(k, tmpm);
  }
  std::map<QString, QString> dk;
  if(!addDuplicate)
  {
    PinCell * existing = dynamic_cast<PinCell *>(pinLib->getEquivelent(pc));
    if(existing != NULL)
    {
      delete pc;
      return existing;
    }
  }
  pinLib->addPart(pc, dk, nc);
  return pc;
}

DuctCell * cmbNucImporter::addDuct(DuctCell * dc, double dh, double dt[2],
                                   std::map<QString, QString> & nc, bool addDuplicate)
{
  dc->setDuctThickness(dt[0], dt[1]);
  dc->setLength(dh);

  cmbNucPartLibrary * ductLib = mainWindow->NuclearCore->getDuctLibrary();

  //adjust materials
  for(unsigned int k = 0; k < dc->numberOfDucts(); ++k)
  {
    Duct * d = dc->getDuct(k);
    int layers = static_cast<int>(d->NumberOfLayers());
    for(int l = 0; l < layers; ++l)
    {
      qDebug() << d->getMaterial(l)->getName() << d->getMaterial(l)->getLabel();
      QPointer<cmbNucMaterial> tmpm = this->getMaterial(d->getMaterial(l));
      assert(tmpm != NULL);
      qDebug() << tmpm->getName() << tmpm->getLabel();
      assert(tmpm != cmbNucMaterialColors::instance()->getUnknownMaterial() ||
             d->getMaterial(l)->getName() == tmpm->getName());
      d->setMaterial(l, tmpm);
      if(mainWindow->NuclearCore->IsHexType())
      {
        double * tmp = d->getNormThick(l);
        tmp[1] = tmp[0];
      }
    }
  }

  if(!addDuplicate)
  {
    DuctCell * existing = dynamic_cast<DuctCell *>(ductLib->getEquivelent(dc));
    if(existing != NULL)
    {
      delete dc;
      return existing;
    }
  }
  
  std::map<QString, QString> dk;
  ductLib->addPart(dc, nc, dk);
  return dc;
}

QPointer<cmbNucMaterial> cmbNucImporter::getMaterial(QPointer<cmbNucMaterial> cm)
{
  cmbNucMaterialColors * matColorMap = cmbNucMaterialColors::instance();
  if( matColorMap->nameUsed(cm->getName()))
  {
    return matColorMap->getMaterialByName(cm->getName());
  }
  else if(matColorMap->labelUsed(cm->getLabel()))
  {
    return matColorMap->getMaterialByLabel(cm->getLabel());
  }
  else
  {
    return  matColorMap->AddMaterial(cm->getName(), cm->getLabel(), cm->getColor());
  }
}

QStringList cmbNucImporter::getXMLFiles()
{
  // Use cached value for last used directory if there is one,
  // or default to the user's home dir if not.
  QSettings settings("CMBNuclear", "CMBNuclear");
  QDir dir = settings.value("cache/lastDir", QDir::homePath()).toString();

  QStringList fileNames =
  QFileDialog::getOpenFileNames(mainWindow, "Open File...", dir.path(), "RGG XML File (*.RXF)");
  if(fileNames.count()==0)
  {
    return fileNames;
  }

  // Cache the directory for the next time the dialog is opened
  QFileInfo info(fileNames[0]);
  settings.setValue("cache/lastDir", info.dir().path());
  return fileNames;
}
