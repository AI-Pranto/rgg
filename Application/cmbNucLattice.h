#ifndef __cmbNucLattice_h__
#define __cmbNucLattice_h__

#include <QColor>
#include <QStringList>

#include <vector>
#include <string>
#include <map>
#include <cassert>
#include <utility>

#include "cmbNucPart.h"

class DrawLatticeItem;
class cmbNucDraw2DLattice;
class cmbLaticeFillFunction;
class cmbNucMaxRadiusFunction;

class Lattice
{
public:
  friend class DrawLatticeItem;
  friend class cmbNucDraw2DLattice;

  enum CellDrawMode{RECT = -1, HEX_FULL = 0, HEX_FULL_30 = 1,
                    HEX_SIXTH_FLAT_BOTTOM, HEX_SIXTH_FLAT_CENTER, HEX_SIXTH_FLAT_TOP,
                    HEX_SIXTH_VERT_BOTTOM, HEX_SIXTH_VERT_CENTER, HEX_SIXTH_VERT_TOP,
                    HEX_TWELFTH_BOTTOM,    HEX_TWELFTH_CENTER,    HEX_TWELFTH_TOP };

  static QString generate_string(QString, CellDrawMode);

  // Represents a cell in the lattice view widget, containing
  // a label and a color.
  struct Cell
  {
    Cell( cmbNucPart * p = NULL) : count(0), part(p) {}
    Cell( Cell const& other)
      : count(0), part(other.part)
    { }

    bool isBlank() const  { return this->getLabel() == "XX"; }

    QString getLabel() const  { return (this->isValid())? part->getLabel(): ""; }
    QColor getColor() const  { return (this->isValid())? part->GetLegendColor():Qt::black; }

    bool isValid() const  { return this->part != NULL; }

    void setPart(cmbNucPart * p)  { this->part = p; }
    cmbNucPart * getPart() const { return this->part; }

    void inc()  { count++; }
    void dec()  { count--; }

    unsigned int getCount() const {return count;}
  protected:
    unsigned int count;
    cmbNucPart * part;
  };

  Lattice();

  Lattice( Lattice const& other );

  ~Lattice();

  Lattice& operator=(Lattice const& arg);

  void setInvalidCells();

  // Sets the dimensions of the cell assembly.
  // For Hex type, i is number of layers, j will be ignored.
  // To force a resize of all layers, pass reset=true
  void SetDimensions(size_t i, size_t j, bool reset = false);

  // Returns the dimensions of the cell assembly.
  // For Hex type, first is number of layers, second is set to 6 (for hex)
  std::pair<size_t, size_t> GetDimensions() const;

  // Sets the contents of the cell (i, j) to name.
  // For Hex type, i is layer/ring index, j is index on that layer
  bool SetCell(size_t i, size_t j, cmbNucPart* part, bool valid = true, bool overInvalid = true);

  // Returns the contents of the cell (i, j).
  // For Hex type, i is layer/ring index, j is index on that layer
  Cell GetCell(size_t i, size_t j) const;

  // Clears the contents of the cell (i, j).
  // For Hex type, i is layer/ring index, j is index on that layer
  void ClearCell(size_t i, size_t j);
  bool ClearCell(const QString &label); //TODO

  bool getValidRange(size_t layer, size_t & start, size_t & end) const;

  bool replacePart(cmbNucPart * oldObj, cmbNucPart * newObj);

  bool deletePart(cmbNucPart * obj);

  std::vector<cmbNucPart *> getUsedParts() const;

  // get/set Geometry type (hex or rect)
  void SetGeometryType(enumGeometryType type);

  enumGeometryType GetGeometryType();

  void SetGeometrySubType(int type);
  int GetGeometrySubType() const;

  size_t getSize() const
  {return Grid.size();}
  size_t getSize(size_t i)const
  { return Grid[i].size(); }

  Lattice::CellDrawMode getDrawMode(size_t index, size_t layer) const;

  void setFullCellMode(Lattice::CellDrawMode m)
  {
    if(this->enGeometryType == HEXAGONAL)
    {
      FullCellMode = m;
    }
    else
    {
      FullCellMode = RECT;
    }
  }

  Lattice::CellDrawMode getFullCellMode() const
  {
    assert(FullCellMode == RECT || FullCellMode == HEX_FULL || FullCellMode == HEX_FULL_30);
    return FullCellMode;
  }

  bool fill(cmbLaticeFillFunction * fun, cmbNucPart * obj);

  enum changeMode {Same = 0, SizeDiff = 1, ContentDiff = 2};
  changeMode compair(Lattice const& other) const;

  void selectCell(size_t i, size_t j);
  void unselect();
  void clearSelection();
  void functionPreview(cmbLaticeFillFunction * fun, cmbNucPart * p);
  void setPotentialConflictCells(double r);

  void setRadius(double r)
  {
    this->radius = r;
  }
  void checkCellRadiiForValidity();

  void updatePitchForMaxRadius(double x, double y);
  void updateMaxRadius(double ri, double rj);
  void sendMaxRadiusToReference();

protected:
  class CellReference
  {
  public:

    enum DrawMode{SELECTED = 1, UNSELECTED = 0, NORMAL=2, FUNCTION_APPLY=3};
    CellReference();
    CellReference(CellReference const& other);
    ~CellReference();

    bool setCell(Cell * c);

    Cell * getCell();
    Cell const* getCell() const;

    Cell * getPreviewCell();
    Cell const* getPreviewCell() const;

    DrawMode getDrawMode() const { return mode; }
    void setDrawMode(DrawMode m) const { mode = m; }
    void setPreviewCell(Cell*pv)
    { previewCell = pv; }
    void clearPreview()
    {
      previewCell = cell;
    }

    Cell const* getModeCell() const;

    void setMaxRadius(double r)
    {
      this->maxRadius = r;
    }
    double getMaxRadius() const
    {
      return maxRadius;
    }

    bool isInConflict() const;

    bool radiusConflicts(double r) const;

    bool isOccupied() const;
    bool hasOccupiers() const
    {
      return !cellOccupier.empty();
    }

    void addOverflow(CellReference* ref, size_t i, size_t j );

    struct OverflowPartReference;

    std::vector<CellReference::OverflowPartReference *> const& getOccupiers() const;
    void clearOverflow();

  protected:
    Cell * cell;
    Cell * previewCell;

    std::vector<OverflowPartReference *> cellOccupier;

    mutable DrawMode mode;
    double maxRadius;
  };

  Cell * getCell(cmbNucPart* part);

  void setUpGrid(Lattice const & other);

  CellReference const& getRerference(int i, int j)
  {
    return this->Grid[i][j];
  }

  std::vector< std::vector<CellReference> > Grid;
  std::map<cmbNucPart*, Cell*> PartToCell;
  enumGeometryType enGeometryType;
  int subType;
  std::vector< std::pair<size_t, size_t> > validRange;
  void computeValidRange();
  Lattice::CellDrawMode FullCellMode;
  cmbNucPart * Blank;
  double radius;
  cmbNucMaxRadiusFunction* maxRadiusFun;
};

class LatticeContainer: public cmbNucPart
{
public:
  LatticeContainer(QString label, QString name, QColor color, double px, double py)
  :cmbNucPart(label, name, color), pitchX(px), pitchY(py)
  {}
  Lattice & getLattice()
  { return this->lattice; }
  virtual QString extractLabel(QString const&) = 0;
  virtual void fillList(std::vector< std::pair<QString, cmbNucPart *> > & l) = 0;
  virtual cmbNucPart * getFromLabel(const QString &) = 0;
  virtual bool IsHexType() = 0;
  virtual void calculateExtraTranslation(double & transX, double & transY) = 0;
  virtual void calculateTranslation(double & transX, double & transY) = 0;
  virtual void setUpdateUsed() = 0;
  virtual void updateLaticeFunction();
  virtual void getRadius(double & ri, double & rj) const;
  virtual double getRadius() const{ return -1; }
  double getPitchX() const
  {
    return this->pitchX;
  }
  double getPitchY() const
  {
    return this->pitchY;
  }
  void setPitch(double x, double y)
  {
    this->pitchX = x; this->pitchY = y;
  }
  std::pair<int, int> GetDimensions()
  {
    return this->lattice.GetDimensions();
  }
protected:
  Lattice lattice;
  double pitchX;
  double pitchY;
};

#endif
