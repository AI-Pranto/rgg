
#include "cmbNucInputPropertiesWidget.h"
#include "ui_qInputPropertiesWidget.h"
#include "ui_qblankwidget.h"
#include "cmbNucWidgetChangeCheckingData.h"

#include "cmbNucAssembly.h"
#include "cmbNucAssemblyLink.h"
#include "cmbNucColorButton.h"
#include "cmbNucCore.h"
#include "cmbNucPinCellEditor.h"
#include "cmbNucDuctCellEditor.h"
#include "cmbNucPinCell.h"
#include "cmbNucMaterialColors.h"
#include "cmbNucMainWindow.h"
#include "cmbCoreParametersWidget.h"
#include "cmbAssyParametersWidget.h"
#include "cmbNucDefaultWidget.h"
#include "cmbNucDefaults.h"
#include "cmbNucWidgetChangeChecker.h"
#include "cmbNucReactorVesselWidget.h"
#include "cmbNucBoundaryLayerWidget.h"
#include "cmbNucPartLibrary.h"

#include <QLabel>
#include <QPointer>
#include <QtDebug>
#include <QColorDialog>
#include <QMessageBox>
#include <QComboBox>
#include <QFileInfo>
#include <QDir>
#include <QSettings>
#include <QFileDialog>

#include <cmath>

static const int degreesHex[6] = {-120, -60, 0, 60, 120, 180};
static const int degreesRec[4] = {-90, 0, 90, 180};

extern bool IS_IN_TESTING_MODE;

typedef cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper WRAPPER;

#define set_and_test_for_change(X, Y)\
   change |= ((Y) != (X)); \
   X = (Y)

namespace
{
  class latticeSizeControl: public cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
  {
  public:
    latticeSizeControl(QLabel * xl, QSpinBox * x, QLabel * yl, QSpinBox * y, LatticeContainer* l)
    :xBox(x), yBox(y), xLabel(xl), yLabel(yl), lattice(l)
    {
      changed[0] = false;
      changed[1] = false;
    }

    virtual ~latticeSizeControl()
    {}

    virtual cmbNucWidgetChangeChecker::mode check()
    {
      std::pair<int, int> size = lattice->GetDimensions();
      int x = xBox->value();
      int y = yBox->value();
      if(x != size.first && !changed[0])
      {
        changed[0] = true;
        return cmbNucWidgetChangeChecker::newly_changed;
      }
      else if(x == size.first && changed[0])
      {
        changed[0] = false;
        return cmbNucWidgetChangeChecker::reverted_to_orginal;
      }
      if(y != size.second && !changed[1])
      {
        changed[1] = true;
        return cmbNucWidgetChangeChecker::newly_changed;
      }
      else if(y == size.second && changed[1])
      {
        changed[1] = false;
        return cmbNucWidgetChangeChecker::reverted_to_orginal;
      }
      return cmbNucWidgetChangeChecker::do_nothing;
    }

    virtual void apply()
    {
      //Lattice widget handles this
      changed[0] = false;
      changed[1] = false;
    }

    virtual void reset()
    {
      yLabel->setVisible(!lattice->IsHexType());
      yBox->setVisible(!lattice->IsHexType());
      if(lattice->IsHexType())
      {
        xLabel->setText("Number Of Layers:");
      }
      else
      {
        xLabel->setText("X:");
      }

      xBox->blockSignals(true);
      yBox->blockSignals(true);
      xBox->setValue(lattice->GetDimensions().first);
      yBox->setValue(lattice->GetDimensions().second);
      yBox->blockSignals(false);
      xBox->blockSignals(false);
      changed[0] = false;
      changed[1] = false;
    }

    virtual void disconnect(cmbNucWidgetChangeChecker * cnwcc)
    {
      QObject::disconnect( xBox,  SIGNAL(valueChanged(const QString &)),
                           cnwcc, SLOT(changeDetected()));
      QObject::disconnect( yBox,  SIGNAL(valueChanged(const QString &)),
                           cnwcc, SLOT(changeDetected()));
    }

    virtual void connect(cmbNucWidgetChangeChecker * cnwcc)
    {
      QObject::connect( xBox,  SIGNAL(valueChanged(const QString &)),
                        cnwcc, SLOT(changeDetected()));
      QObject::connect( yBox,  SIGNAL(valueChanged(const QString &)),
                        cnwcc, SLOT(changeDetected()));
    }

    virtual size_t getNumberOfObjects()
    {
      return 2;
    }

    QObject * getObject(size_t i)
    {
      switch(i)
      {
        case 0:
          return xBox;
        case 1:
          return yBox;
      }
      return NULL;
    }
  private:
    QSpinBox * xBox, * yBox;
    QLabel * xLabel, * yLabel;
    LatticeContainer  * lattice;
    bool changed[2];
  };

  const int STILL_DIFFERENT = 1;
  const int STILL_SAME = 3;
  const int CHANGE_TO_DIFFERENT = 7;
  const int CHANGE_TO_SAME = 15;

  class pitchChangeChecker: public cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper
  {
  public:
    pitchChangeChecker( boost::function<void (void)> ap, cmbNucAssembly * a, QCheckBox * cp,
                        QDoubleSpinBox * px, QDoubleSpinBox * py )
    : autoPitch(ap), assembly(a), centerPins(cp), pitchX(px), pitchY(py),  pitchXChanged(false),
      pitchYChanged(false)
    {
      reset();
    }
    virtual ~pitchChangeChecker()
    {
      this->autoPitch = 0;
    }
    virtual cmbNucWidgetChangeChecker::mode check()
    {
      //block signals
      bool oldX = this->pitchX->blockSignals(true);
      bool oldY = this->pitchY->blockSignals(true);
      autoPitch();
      //unblock signals
      this->pitchX->blockSignals(oldX);
      this->pitchY->blockSignals(oldY);
      int changeX = checkX();
      int changeY = checkY();
      int change = changeX + changeY;

      switch(change)
      {
        case STILL_DIFFERENT + STILL_DIFFERENT:
        case STILL_SAME + STILL_SAME:
        case STILL_DIFFERENT + CHANGE_TO_DIFFERENT:
        case STILL_DIFFERENT + CHANGE_TO_SAME:
          return cmbNucWidgetChangeChecker::do_nothing;
        case STILL_SAME + CHANGE_TO_DIFFERENT:
        case CHANGE_TO_DIFFERENT + CHANGE_TO_DIFFERENT:
          return cmbNucWidgetChangeChecker::newly_changed;
        case STILL_SAME + CHANGE_TO_SAME:
        case CHANGE_TO_SAME + CHANGE_TO_SAME:
          return cmbNucWidgetChangeChecker::reverted_to_orginal;
      }
      return cmbNucWidgetChangeChecker::do_nothing;
    }
    virtual void apply()
    {
      this->autoPitch();
      double px = this->pitchX->value();
      double py = this->pitchY->value();
      assembly->setPitch(px,py);

      assembly->setCenterPins(this->centerPins->isChecked());
      pitchXChanged = false;
      pitchYChanged = false;
    }
    virtual void reset()
    {
      this->centerPins->setChecked(assembly->isPinsAutoCentered());
      if(!assembly->isPinsAutoCentered())
      {
        this->pitchX->setValue(assembly->getPitchX());
        this->pitchY->setValue(assembly->getPitchY());
      }
      autoPitch();
      pitchXChanged = false;
      pitchYChanged = false;
    }
    virtual void disconnect(cmbNucWidgetChangeChecker * t)
    {
      QObject::disconnect( centerPins, SIGNAL(stateChanged(int)), t, SLOT(changeDetected()));
      QObject::disconnect( pitchX, SIGNAL(valueChanged(const QString &)),
                           t, SLOT(changeDetected()));
      QObject::disconnect( pitchY, SIGNAL(valueChanged(const QString &)),
                           t, SLOT(changeDetected()));
    }
    virtual void connect(cmbNucWidgetChangeChecker * t)
    {
      QObject::connect( centerPins, SIGNAL(stateChanged(int)), t, SLOT(changeDetected()));
      QObject::connect( pitchX, SIGNAL(valueChanged(const QString &)), t, SLOT(changeDetected()));
      QObject::connect( pitchY, SIGNAL(valueChanged(const QString &)), t, SLOT(changeDetected()));
    }
    virtual size_t getNumberOfObjects()
    {
      return 3;
    }
    virtual QObject * getObject(size_t i)
    {
      switch(i)
      {
        case 0: return centerPins;
        case 1: return pitchX;
        case 2: return pitchY;
      }
      return NULL;
    }

  protected:
    boost::function<void (void)> autoPitch;
    cmbNucAssembly *assembly;
    QCheckBox *centerPins;
    QDoubleSpinBox *pitchX, *pitchY;
    bool pitchXChanged, pitchYChanged;

    int checkX()
    {
      bool pxChanged = this->pitchX->value() != assembly->getPitchX();
      if( pxChanged && !pitchXChanged )
      {
        pitchXChanged = true;
        return CHANGE_TO_DIFFERENT;
      }
      if( !pxChanged && pitchXChanged )
      {
        pitchXChanged = false;
        return CHANGE_TO_SAME;
      }
      return (pitchXChanged)?STILL_DIFFERENT:STILL_SAME;
    }

    int checkY()
    {
      bool pyChanged = this->pitchY->value() != assembly->getPitchY();
      if( pyChanged && !pitchYChanged )
      {
        pitchYChanged = true;
        return CHANGE_TO_DIFFERENT;
      }
      if( !pyChanged && pitchYChanged )
      {
        pitchYChanged = false;
        return CHANGE_TO_SAME;
      }
      return (pitchYChanged)?STILL_DIFFERENT:STILL_SAME;
    }
  };

#define ASSY_UNIQUE_CHECKER(TYPE, FUNCTION)\
class assy##TYPE##Checker: public cmbNucGetSetDataUnqueChecker<QString> \
{ \
public: \
  assy##TYPE##Checker(cmbNucCore *c): core(c) {} \
  virtual bool check(QString const& in) { return core->FUNCTION(in); } \
  virtual void displayMessage(QString const&) { } \
private: \
  cmbNucCore * core; \
};

  ASSY_UNIQUE_CHECKER(Name, name_unique)
  ASSY_UNIQUE_CHECKER(Label, label_unique)

  template<typename AssyType>
  void applyAssyLabel(AssyType * assy, QString new_label)
  {
    QString old_label = assy->getLabel();
    assy->setLabel(new_label);
  }

  bool justReturn(){ return true; }

  void postApplySendLabel(cmbNucInputPropertiesWidget *w, cmbNucPart *obj, bool change)
  {
    if(change)
    {
      w->emitNameChange(obj, QString(obj->getTitle()), QString(obj->getTitle()));
    }
  }

  void postApplyLinkSend(cmbNucInputPropertiesWidget *w, cmbNucAssemblyLink *link, bool change)
  {
    if(change)
    {
      w->emitNameChange(link, link->getName() + " (" + link->getLabel() + ")", link->getTitle());
    }
  }

  QString getRotation( cmbNucAssembly * assy )
  {
    return QString::number(static_cast<int>(assy->getZAxisRotation()));
  }

  void setRotation(cmbNucAssembly * assy, QComboBox * rbox, QString /*deg*/)
  {
    int ind = rbox->currentIndex();
    if (assy->IsHexType())
    {
      if(ind >= 0 && ind < 6)
        assy->setZAxisRotation(degreesHex[ind]);
    }
    else
    {
      if(ind >= 0 && ind < 4)
        assy->setZAxisRotation(degreesRec[ind]);
    }
  }
  QString getCurrentDuctName( cmbNucAssembly * assy )
  {
    return QString(assy->getAssyDuct().getName());
  }
  void setCurrentDuct( cmbNucAssembly * assy, QString name)
  {
    DuctCell * dc = assy->getDuctLibrary()->GetPart<DuctCell>(name, cmbNucPartLibrary::BY_NAME);
    assy->setDuctCell(dc);
  }
  bool checkColor(QColor) {return true;}

  class idUniqueChecker: public cmbNucGetSetDataUnqueChecker<int>
  {
  public:
    idUniqueChecker(cmbNucAssemblyLink *a, cmbNucCore *c)
    : link(a), core(c)
    {}
    virtual bool check(int const& in)
    {
      return core->isFreeId(link, in);
    }
    virtual void displayMessage(int const&)
    {
      //no message for now
    }
  private:
    cmbNucAssemblyLink * link;
    cmbNucCore * core;
  };

#define SET_COLOR_TRACKER(CLASS, BUTTON, OBJ) \
  { \
    cmbNucGetSetData<cmbNucColorButton, QColor, QColor> * tmpData = NULL; \
    boost::function<QColor (void)> getData = boost::bind(&CLASS::GetLegendColor, OBJ); \
    boost::function<void (QColor)> setData = boost::bind(&CLASS::SetLegendColor, OBJ, _1); \
    boost::function<void (void)> setDataInvalid = WRAPPER::nothingToSet; \
    boost::function<bool (void)> dataIsSet = justReturn; \
    boost::function<bool (QColor)> isValidData = checkColor; \
    cmbNucColorButton * widget = BUTTON; \
    boost::function<void (bool)> postApply =  WRAPPER::doNothing<bool>; \
    tmpData = new cmbNucGetSetData<cmbNucColorButton, QColor, QColor>(widget, getData, setData, \
                                                                      setDataInvalid,  dataIsSet, \
                                                                      isValidData, postApply); \
    this->Internal->change_checker.registerToTrack(tmpData); \
  }

#define ASSY_NAME_LABEL(NAME, QOBJECT, CLASS, APPLY_FUN, SET_FUNCTION)\
{ \
  cmbNucGetSetData<QLineEdit, QString, QString> * tmpData = NULL; \
  boost::function<QString (void)> getData = boost::bind(&CLASS::get##NAME, assyObj); \
  boost::function<void (QString)> setData = SET_FUNCTION; \
  boost::function<void (void)> setDataInvalid = WRAPPER::nothingToSet; \
  boost::function<bool (void)> dataIsSet = justReturn; \
  boost::function<bool (QString)> isValidData = WRAPPER::isNotEmtpyString; \
  QLineEdit * widget = this->Internal->QOBJECT; \
  boost::function<void (bool)> postApply = boost::bind(&APPLY_FUN, this, assyObj, _1); \
  tmpData = new cmbNucGetSetData<QLineEdit, QString, QString>(widget, getData, setData, \
                                                              setDataInvalid,  dataIsSet, \
                                                              isValidData, postApply); \
  tmpData->setWidgetToData(WRAPPER::replace_space_converter); \
  tmpData->addUniqueChecker(new assy##NAME##Checker(Core)); \
  this->Internal->change_checker.registerToTrack(tmpData); \
}

}


class cmbNucInputPropertiesWidgetInternal :
  public Ui::InputPropertiesWidget
{
public:
  ~cmbNucInputPropertiesWidgetInternal()
  {
    delete PinCellEditor;
    delete DuctCellEditor;
    delete VesselWidget;
    delete BoundaryWidget;

    delete assemblyColorNucButton;
    delete assemblyLinkColorNucButton;
    delete pincellColorNucButton;
    delete blankColorNucButton;
    delete blank;
  }
  QPointer<cmbNucPinCellEditor> PinCellEditor;
  QPointer<cmbNucDuctCellEditor> DuctCellEditor;
  QPointer<cmbNucReactorVesselWidget> VesselWidget;
  QPointer<cmbNucBoundaryLayerWidget> BoundaryWidget;
  std::string background_full_path;
  cmbNucWidgetChangeChecker change_checker;
  QPointer<cmbNucColorButton> assemblyColorNucButton;
  QPointer<cmbNucColorButton> assemblyLinkColorNucButton;
  QPointer<cmbNucColorButton> pincellColorNucButton;
  QPointer<cmbNucColorButton> blankColorNucButton;
  Ui::blank * blank;
};

//-----------------------------------------------------------------------------
cmbNucInputPropertiesWidget::cmbNucInputPropertiesWidget(cmbNucMainWindow *mainWindow)
  : QWidget(mainWindow),
    MainWindow(mainWindow)
{
  this->Internal = new cmbNucInputPropertiesWidgetInternal;
  this->Internal->setupUi(this);
  this->initUI();
  this->CurrentObject = NULL;
  this->Assembly = NULL;
  this->Core = NULL;
}

//-----------------------------------------------------------------------------
cmbNucInputPropertiesWidget::~cmbNucInputPropertiesWidget()
{
  this->CurrentObject = NULL;
  this->Assembly = NULL;
  this->Core = NULL;
  delete this->Internal;
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::initUI()
{
  this->CoreProperties = new cmbCoreParametersWidget(this);
  this->Internal->CoreConfLayout->addWidget(this->CoreProperties);
  connect(this->CoreProperties, SIGNAL(valuesChanged()),
          this, SIGNAL(valuesChanged()));

  this->assyConf = new cmbAssyParametersWidget(this);
  this->Internal->AssyConfLayout->addWidget(this->assyConf);
  connect(this->assyConf, SIGNAL(valuesChanged()),
          this, SIGNAL(valuesChanged()));

  QObject::connect(this->Internal->ApplyButton, SIGNAL(clicked()), this, SLOT(onApply()));
  QObject::connect(this->Internal->ResetButton, SIGNAL(clicked()), this, SLOT(onReset()));
  QObject::connect(this->Internal->ResetButton, SIGNAL(clicked()), this, SIGNAL(reset()));

  QObject::connect(this->Internal->latticeX, SIGNAL(valueChanged(int)),
                   this, SLOT(xSizeChanged(int)));
  QObject::connect(this->Internal->latticeY, SIGNAL(valueChanged(int)),
                   this, SLOT(ySizeChanged(int)));
  QObject::connect(this->Internal->coreLatticeX, SIGNAL(valueChanged(int)),
                   this, SIGNAL(sendXSize(int)));
  QObject::connect(this->Internal->coreLatticeY, SIGNAL(valueChanged(int)),
                   this, SIGNAL(sendYSize(int)));

  QObject::connect(this->Internal->pitchX, SIGNAL(valueChanged(double)),
                   this, SLOT(sendPitchChange()));
  QObject::connect(this->Internal->pitchY, SIGNAL(valueChanged(double)),
                   this, SLOT(sendPitchChange()));

  this->Internal->assemblyColorNucButton = new cmbNucColorButton(this->Internal->assyColorButton);
  this->Internal->assemblyLinkColorNucButton =
                                        new cmbNucColorButton(this->Internal->assyLinkColorButton);
  this->Internal->pincellColorNucButton = new cmbNucColorButton(this->Internal->pincellColorButton);

  CoreDefaults = new cmbNucDefaultWidget();
  this->Internal->CoreDefaults->addWidget(CoreDefaults);

  connect( CoreDefaults, SIGNAL(commonChanged()),
           this,         SIGNAL(valuesChanged()));

  connect( this->Internal->computePitch, SIGNAL(clicked()),
           this,                         SLOT(computePitch()));

  //CORE:
  cmbNucWidgetChangeChecker * checker = &(this->Internal->change_checker);

  this->Internal->VesselWidget = new cmbNucReactorVesselWidget(this,
                                                               this->Internal->coreLatticeX,
                                                               this->Internal->coreLatticeY);
  this->Internal->OuterJacketLayout->addWidget(this->Internal->VesselWidget);
  {
    cmbNucReactorVesselWidget * vessel = this->Internal->VesselWidget;
    connect( checker, SIGNAL(sendApply()), vessel, SLOT(apply()) );
    connect( checker, SIGNAL(sendReset()), vessel, SLOT(reset()) );
    connect( checker, SIGNAL(sendClear()), vessel, SLOT(clear()) );
    connect( vessel,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
             checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)) );
    connect( vessel,  SIGNAL(drawCylinder(double, int)), this, SIGNAL(drawCylinder(double, int)) );
  }

  this->Internal->BoundaryWidget = new cmbNucBoundaryLayerWidget(this);
  this->Internal->boundaryLayerLayout->addWidget(this->Internal->BoundaryWidget);
  {
    cmbNucBoundaryLayerWidget * vessel = this->Internal->BoundaryWidget;
    connect( checker, SIGNAL(sendApply()), vessel, SLOT(apply()) );
    connect( checker, SIGNAL(sendReset()), vessel, SLOT(reset()) );
    connect( checker, SIGNAL(sendClear()), vessel, SLOT(clear()) );
    connect( vessel,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
             checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)) );
  }

  //pincell
  this->Internal->PinCellEditor = new cmbNucPinCellEditor(this);
  this->Internal->pinEditorContainer->addWidget(this->Internal->PinCellEditor);
  {
    cmbNucPinCellEditor * editor = this->Internal->PinCellEditor;
    connect( editor, SIGNAL(pincellModified(cmbNucPart*)),
             this,  SIGNAL(objGeometryChanged(cmbNucPart*)) );
    connect( editor, SIGNAL(resetView()), this, SIGNAL(resetView()));
    connect( editor, SIGNAL(valueChange()), this, SIGNAL(valuesChanged()) );
    connect( editor, SIGNAL(nameChange(cmbNucPart*, const QString &, const QString &)),
             this,   SLOT(emitNameChange(cmbNucPart*, const QString &, const QString &)));
    connect( checker, SIGNAL(sendApply()), editor, SLOT(apply()) );
    connect( checker, SIGNAL(sendReset()), editor, SLOT(reset()) );
    connect( checker, SIGNAL(sendClear()), editor, SLOT(clear()) );
    connect( editor,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
             checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)) );
  }

  //ductcell
  {
    this->Internal->DuctCellEditor = new cmbNucDuctCellEditor(this);
    cmbNucDuctCellEditor * editor = this->Internal->DuctCellEditor;
    this->Internal->ductEditorContainer->addWidget(this->Internal->DuctCellEditor);
    connect( editor, SIGNAL(ductcellModified(cmbNucPart*)),
             this,   SIGNAL(objGeometryChanged(cmbNucPart*)));
    connect( editor, SIGNAL(nameChanged(cmbNucPart*, QString, QString)),
             this,   SLOT(emitNameChange(cmbNucPart*, const QString &, const QString &)));
    connect( checker, SIGNAL(sendApply()), editor, SLOT(apply()) );
    connect( checker, SIGNAL(sendReset()), editor, SLOT(reset()) );
    connect( checker, SIGNAL(sendClear()), editor, SLOT(clear()) );
    connect( editor,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
             checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)) );
  }
  connect( CoreDefaults,  SIGNAL(widthChanged(double)),
           this->Internal->VesselWidget, SLOT(setWidth(double)) );
  connect( CoreDefaults,  SIGNAL(heightChanged(double)),
           this->Internal->VesselWidget, SLOT(setHeight(double)) );

  this->Internal->blank = new Ui::blank();
  this->Internal->blank->setupUi(this->Internal->pageBlank);
  this->Internal->blankColorNucButton = new cmbNucColorButton(this->Internal->blank->Color);
}

//-----------------------------------------------------------------------------

bool cmbNucInputPropertiesWidget::checkForApply()
{
  if(this->Internal->change_checker.needToApply()) return true;
  return false;
}

void cmbNucInputPropertiesWidget::clearChangeMode()
{
  this->Internal->change_checker.clear();
}

//-----------------------------------------------------------------------------
bool cmbNucInputPropertiesWidget::ductCellIsCrossSectioned()
{
  return this->Internal->DuctCellEditor->isCrossSectioned();
}

//-----------------------------------------------------------------------------
bool cmbNucInputPropertiesWidget::pinCellIsCrossSectioned()
{
  return this->Internal->PinCellEditor->isCrossSectioned();
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::setObject(cmbNucPart* selObj, const char* name)
{
  this->CurrentObject = selObj;
  this->Internal->PinCellEditor->SetPinCell(NULL, this->Core);
  this->Internal->DuctCellEditor->SetDuctCell(NULL, this->Core);
  emit(sendLattice(NULL));
  if(!selObj)
  {
    emit currentObjectNameChanged("");
    this->setEnabled(false);
    this->Internal->stackedWidget->setCurrentWidget(this->Internal->emptyPage);
    return;
  }
  //set up the checker
  switch(this->CurrentObject->GetType())
  {
    case CMBNUC_CORE:
    {
      cmbNucCore* nucCore = dynamic_cast<cmbNucCore*>(selObj);
      this->Core = nucCore;
      this->Internal->VesselWidget->set(this->Core);
      this->Internal->BoundaryWidget->set(this->Core);
      this->CoreProperties->setCore(nucCore, this->Internal->change_checker);
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pageCore);
      this->CoreDefaults->set(nucCore, this->Internal->change_checker);
      this->Internal->change_checker.registerToTrack(
                                      new latticeSizeControl(this->Internal->coreLabelX,
                                                             this->Internal->coreLatticeX,
                                                             this->Internal->coreLabelY,
                                                             this->Internal->coreLatticeY,
                                                             dynamic_cast<cmbNucCore*>(selObj)));
      break;
    }
    case CMBNUC_ASSEMBLY:
    {
      cmbNucAssembly* assy = dynamic_cast<cmbNucAssembly*>(selObj);
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pageAssembly);
      this->setAssembly(assy);
      break;
    }
    case CMBNUC_ASSEMBLY_LINK:
    {
      cmbNucAssemblyLink* link = dynamic_cast<cmbNucAssemblyLink*>(selObj);
      this->setAssemblyLink(link);
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pageLink);
      break;
    }
    case CMBNUC_ASSY_PINCELL:
    {
      PinCell * pc = dynamic_cast<PinCell*>(selObj);
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pagePinCell);
      SET_COLOR_TRACKER(PinCell, this->Internal->pincellColorNucButton, pc)
      this->Internal->PinCellEditor->SetPinCell(pc, this->Core);
      break;
    }
    case CMBNUC_ASSY_DUCTCELL:
    {
      DuctCell * dc = dynamic_cast<DuctCell*>(selObj);
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pageDuctCell);
      this->Internal->DuctCellEditor->SetDuctCell(dc, this->Core);
      break;
    }
    case CMBNUC_ASSY_BASEOBJ:
    {
      cmbNucPart* assyObj = selObj;
      this->Internal->stackedWidget->setCurrentWidget(this->Internal->pageBlank);
      ASSY_NAME_LABEL(Name, blank->Name, cmbNucPart, postApplySendLabel,
                      boost::bind(&cmbNucAssembly::setName BOOST_PP_COMMA()
                                  selObj BOOST_PP_COMMA() _1))
      ASSY_NAME_LABEL(Label, blank->Label, cmbNucPart, postApplySendLabel,
                      boost::bind(&applyAssyLabel<cmbNucPart> BOOST_PP_COMMA()
                                  selObj BOOST_PP_COMMA() _1))
      SET_COLOR_TRACKER(cmbNucPart, this->Internal->blankColorNucButton, selObj)
      break;
    }
  }
  this->setEnabled(true);
  if(name != NULL)
  {
    emit currentObjectNameChanged(selObj->getTitle());
  }
  else
  {
    emit currentObjectNameChanged("");
  }

  this->onReset();
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::setAssembly(cmbNucAssembly *assyObj)
{
  this->Assembly = assyObj;
  if(assyObj == NULL) return;

  this->Internal->pitchY->setVisible(!assyObj->IsHexType());
  this->Internal->xlabel->setVisible(!assyObj->IsHexType());
  this->Internal->ylabel->setVisible(!assyObj->IsHexType());

  assyConf->setAssembly(assyObj, Core, this->Internal->change_checker);
  //AssyName
  ASSY_NAME_LABEL(Name, AssyName, cmbNucAssembly, postApplySendLabel,
                  boost::bind(&cmbNucAssembly::setName BOOST_PP_COMMA()
                               this->Assembly BOOST_PP_COMMA() _1))
  ASSY_NAME_LABEL(Label, AssyLabel, cmbNucAssembly, postApplySendLabel,
                  boost::bind(&applyAssyLabel<cmbNucAssembly> BOOST_PP_COMMA()
                              assyObj BOOST_PP_COMMA() _1))

  //this->Internal->assyColorButton
  SET_COLOR_TRACKER(cmbNucAssembly, this->Internal->assemblyColorNucButton, this->Assembly)

  this->Internal->change_checker.registerToTrack(
          new latticeSizeControl(this->Internal->AssemblyLabelX, this->Internal->latticeX,
                                 this->Internal->AssemblyLabelY, this->Internal->latticeY,
                                 this->Assembly));

  //pitchX, pitchY
  boost::function<void (void)> ap = boost::bind(&cmbNucInputPropertiesWidget::autoPitch, this);
  this->Internal->change_checker.registerToTrack(
        new pitchChangeChecker( ap, assyObj, this->Internal->CenterPins, this->Internal->pitchX,
                                this->Internal->pitchY ));

  // this->Internal->Ducts
  this->Internal->Ducts->clear();
  std::vector< std::pair<QString, cmbNucPart *> > list;
  assyObj->getDuctLibrary()->fillListJustName(list);
  for(unsigned int i = 0; i < list.size(); ++i)
  {
    this->Internal->Ducts->addItem(list[i].first);
  }
  {
    cmbNucGetSetData<QComboBox, QString, QString> * tmpData;
    QComboBox * widget = this->Internal->Ducts;
    boost::function<QString (void)> getData = boost::bind(&getCurrentDuctName, this->Assembly);
    boost::function<void (QString)> setData = boost::bind(&setCurrentDuct, this->Assembly, _1);
    boost::function<void (void)> setDataInvalid = WRAPPER::nothingToSet;
    boost::function<bool (void)> dataIsSet = justReturn;
    boost::function<bool (QString)> isValidData = WRAPPER::isNotEmtpyString;
    boost::function<void (bool)> postApply = WRAPPER::doNothing<bool>;
    tmpData = new cmbNucGetSetData<QComboBox, QString, QString>( widget, getData, setData,
                                                                 setDataInvalid,  dataIsSet,
                                                                 isValidData, postApply);
    this->Internal->change_checker.registerToTrack(tmpData);
  }

  //rotationDegree
  this->Internal->rotationDegree->clear();
  if (Assembly->IsHexType())
  {
    for(unsigned int i = 0; i < 6; ++i)
    {
      this->Internal->rotationDegree->addItem(QString::number(degreesHex[i]));
    }
  }
  else
  {
    for(unsigned int i = 0; i < 4; ++i)
    {
      this->Internal->rotationDegree->addItem(QString::number(degreesRec[i]));
    }
  }

  {
    cmbNucGetSetData<QComboBox, QString, QString> * tmpData;
    QComboBox * widget = this->Internal->rotationDegree;
    boost::function<QString (void)> getData = boost::bind(&getRotation, this->Assembly);
    boost::function<void (QString)> setData = boost::bind(&setRotation, this->Assembly, widget, _1);
    boost::function<void (void)> setDataInvalid = WRAPPER::nothingToSet;
    boost::function<bool (void)> dataIsSet = justReturn;
    boost::function<bool (QString)> isValidData = WRAPPER::isNotEmtpyString;
    boost::function<void (bool)> postApply = WRAPPER::doNothing<bool>;
    tmpData = new cmbNucGetSetData<QComboBox, QString, QString>( widget, getData, setData,
                                                                 setDataInvalid,  dataIsSet,
                                                                 isValidData, postApply);
    this->Internal->change_checker.registerToTrack(tmpData);
  }
  //
}

void cmbNucInputPropertiesWidget::setAssemblyLink(cmbNucAssemblyLink* assyObj)
{
  //AssyName
  ASSY_NAME_LABEL(Name, AssyLinkName, cmbNucAssemblyLink, postApplyLinkSend,
                  boost::bind( &cmbNucAssemblyLink::setName BOOST_PP_COMMA()
                               assyObj BOOST_PP_COMMA() _1))
  ASSY_NAME_LABEL(Label, AssyLinkLabel, cmbNucAssemblyLink, postApplyLinkSend,
                  boost::bind(&applyAssyLabel<cmbNucAssemblyLink> BOOST_PP_COMMA()
                              assyObj BOOST_PP_COMMA() _1))

#define CHECKERFUN(NAME, DATA_TYPE, WIDGET, W_TYPE, IS_VALID_DATA)                                 \
{                                                                                                  \
  boost::function<DATA_TYPE (void)> getData = boost::bind(&cmbNucAssemblyLink::get##NAME, assyObj);\
  boost::function<void (DATA_TYPE)> setData =                                                      \
                                          boost::bind(&cmbNucAssemblyLink::set##NAME, assyObj, _1);\
  boost::function<void (void)> setDataInvalid = WRAPPER::nothingToSet;                             \
  boost::function<bool (void)> dataIsSet = justReturn;                                             \
\
  boost::function<bool (W_TYPE)> isValidData = IS_VALID_DATA;                                      \
\
  WIDGET * widget = this->Internal->NAME;                                                          \
  boost::function<void (bool)> postApply = WRAPPER::doNothing<bool>;                               \
  cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE> * tmpData =                                          \
          new cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE>(widget, getData, setData, setDataInvalid,\
                                                          dataIsSet, isValidData, postApply);      \
  tmpData->addUniqueChecker(new idUniqueChecker(assyObj, Core));                                   \
  this->Internal->change_checker.registerToTrack(tmpData);                                         \
}
  CHECKERFUN(MaterialStartID, int, QLineEdit, QString, WRAPPER::isNumber)
  CHECKERFUN(NeumannStartID, int, QLineEdit, QString, WRAPPER::isNumber)

  SET_COLOR_TRACKER(cmbNucAssemblyLink, this->Internal->assemblyLinkColorNucButton, assyObj)
}

// Invoked when Apply button clicked
//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::onApply()
{
  if(this->CurrentObject == NULL)
  {
    return;
  }
  cmbNucPart* selObj = this->CurrentObject;
  bool changed = this->Internal->change_checker.needToApply();
  this->Internal->change_checker.apply();
  if(changed)
  {
    emit this->objGeometryChanged(selObj);
    this->Core->setAndTestDiffFromFiles(true);
    selObj->sendChanged();
    emit valuesChanged();
  }
  switch(selObj->GetType())
  {
    case CMBNUC_CORE:
    {
      if(this->CoreDefaults->needCameraReset())  emit resetView();
      break;
    }
    case CMBNUC_ASSEMBLY:
    case CMBNUC_ASSY_PINCELL:
    case CMBNUC_ASSY_DUCTCELL:
    case CMBNUC_ASSEMBLY_LINK:
    case CMBNUC_ASSY_BASEOBJ:
      break;
    default:
      this->setEnabled(false);
      break;
  }
}

// Invoked when Reset button clicked
//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::onReset()
{
  if(this->CurrentObject == NULL)
  {
    return;
  }
  cmbNucPart* selObj = this->CurrentObject;
  this->Internal->change_checker.reset();
  switch(selObj->GetType())
  {
    case CMBNUC_CORE:
    case CMBNUC_ASSEMBLY:
      emit(sendLattice( dynamic_cast<LatticeContainer*>(selObj) ));
      break;
    case CMBNUC_ASSEMBLY_LINK:
      emit(sendLattice( dynamic_cast<cmbNucAssemblyLink*>(selObj)->getLink()) );
      break;
    case CMBNUC_ASSY_PINCELL:
    case CMBNUC_ASSY_DUCTCELL:
      emit(select3DModelView());
      emit(sendLattice(NULL));
      break;
    case CMBNUC_ASSY_BASEOBJ:
      break;
    default:
      emit(sendLattice(NULL));
      this->setEnabled(false);
      break;
  }
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::clear()
{
  this->setObject(NULL,NULL);
  this->Internal->stackedWidget->setCurrentWidget(this->Internal->emptyPage);
  this->Internal->change_checker.clear();
  this->setAssembly(NULL);
}

void cmbNucInputPropertiesWidget::colorChanged()
{
  if(this->CurrentObject == NULL)
  {
    return;
  }
  cmbNucPart* selObj = this->CurrentObject;
  switch(selObj->GetType())
  {
    case CMBNUC_ASSEMBLY_LINK:
      selObj = dynamic_cast<cmbNucAssemblyLink*>(selObj)->getLink();
    case CMBNUC_CORE:
    case CMBNUC_ASSEMBLY:
      emit objGeometryChanged(selObj);
      break;
    case CMBNUC_ASSY_PINCELL:
      this->Internal->PinCellEditor->UpdateData();
      break;
    case CMBNUC_ASSY_DUCTCELL:
      this->Internal->DuctCellEditor->update();
      break;
    case CMBNUC_ASSY_BASEOBJ:
      break;
  }
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::xSizeChanged(int i)
{
  this->autoPitch();
  emit sendXSize(i);
}

void cmbNucInputPropertiesWidget::ySizeChanged(int i)
{
  this->autoPitch();
  emit sendYSize(i);
}

//-----------------------------------------------------------------------------
void cmbNucInputPropertiesWidget::computePitch()
{
  double px, py;
  if(this->CurrentObject == NULL) return;
  cmbNucAssembly* assy = dynamic_cast<cmbNucAssembly*>(this->CurrentObject);
  if(assy == NULL) return;
  assy->calculatePitch( this->Internal->latticeX->value(), this->Internal->latticeY->value(),
                        px, py );
  this->Internal->pitchX->setValue(px);
  this->Internal->pitchY->setValue(py);
  emit(pitchChanged(px, py));
}

void cmbNucInputPropertiesWidget::sendPitchChange()
{
  emit(pitchChanged(this->Internal->pitchX->value(), this->Internal->pitchY->value()));
}

void cmbNucInputPropertiesWidget::connectLatticeWidget(cmbNucDraw2DLattice * w)
{
  cmbNucWidgetChangeChecker * checker = &(this->Internal->change_checker);
  connect( checker, SIGNAL(sendApply()), w, SLOT(apply()) );
  connect( checker, SIGNAL(sendReset()), w, SLOT(reset()) );
  connect( checker, SIGNAL(sendClear()), w, SLOT(clear()) );
  connect( w,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
           checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)) );
}

void cmbNucInputPropertiesWidget::deleteObj(cmbNucPart * obj)
{
  emit(sendLattice(NULL));
  if(this->CurrentObject == obj)
  {
    this->CurrentObject = NULL;
    this->Internal->change_checker.clear();
  }
  delete obj;
}

void cmbNucInputPropertiesWidget::autoPitch()
{
  bool v = this->Internal->CenterPins->isChecked();
  if(v)
  {
    this->computePitch();
  }
  this->Internal->pitchX->setEnabled( !v );
  this->Internal->pitchY->setEnabled( !v );
  this->Internal->computePitch->setEnabled( !v);
}
