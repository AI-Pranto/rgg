#include "cmbNucCordinateConverter.h"
#include "cmbNucAssembly.h"
#include "vtkMath.h"
#include <cmath>
#include <cassert>
#include <QDebug>

const double cmbNucMathConst::cos30        = 0.86602540378443864676372317075294;
const double cmbNucMathConst::cos30Squared = 0.86602540378443864676372317075294*
                                             0.86602540378443864676372317075294;

const double cmbNucMathConst::radians60    = vtkMath::Pi()/3.0;

const double cmbNucMathConst::cosSinAngles[6][2] =
  { { -0.5, -cmbNucMathConst::cos30 }, {  0.5, -cmbNucMathConst::cos30 }, {  1.0, 0.0 },
    {  0.5,  cmbNucMathConst::cos30 }, { -0.5,  cmbNucMathConst::cos30 }, { -1.0, 0.0 } };

class cmbNucCordinateConverterInternal
{
  friend class cmbNucCordinateConverter;
protected:
  cmbNucCordinateConverterInternal(Lattice & g):Grid(g){}
  virtual ~cmbNucCordinateConverterInternal(){}
  virtual void convertToPixelXY(int i, int j, double & x, double & y, double r1, double r2) = 0;
  virtual void computeRadius(int w, int h, double r[2]) = 0;
  Lattice & Grid;
};

namespace
{
  class convertHex: public cmbNucCordinateConverterInternal
  {
    friend class cmbNucCordinateConverter;
  public:
    convertHex(Lattice & g, bool ignoreRotation):cmbNucCordinateConverterInternal(g)
    {
      assert( this->Grid.GetGeometryType() == HEXAGONAL );
      static const int indx[] = { 0,1,2,3,4,5,0,5,4,3,2,1,0};
      int const* off1 = indx;
      int const* off2 = indx;
      if(!ignoreRotation)
      {
        if( Grid.GetGeometrySubType() & ANGLE_60 &&
           Grid.GetGeometrySubType() & VERTEX )
        {
          off1 += 7;
          off2 += 11;
        }
        else if(Grid.GetGeometrySubType() & ANGLE_360 &&
                Grid.getFullCellMode() == Lattice::HEX_FULL_30)
        {
          off1 += 6;
          off2 += 11;
        }
      }
      for(int c = 0; c < 6; c++)
      {
        int ca = off1[c];
        corner[c][0] = cmbNucMathConst::cosSinAngles[ca][ off2[0] ];
        corner[c][1] = cmbNucMathConst::cosSinAngles[ca][ off2[1] ];
      }
      for(int c = 0; c < 6; c++)
      {
        int cp1 = (c+1)%6;
        dir[c][0] = (corner[cp1][0] - corner[c][0]);
        dir[c][1] = (corner[cp1][1] - corner[c][1]);
      }
    }
    virtual void convertToPixelXY(int i, int j, double & x, double & y, double r1, double /*r2*/)
    {
      if(i == 0)
      {
        x = 0;
        y = 0;
        return;
      }
      double layerRadius = r1 * i;
      int s = j/i;
      int sr = j%i;
      double deltax = dir[s][0]*layerRadius/i;
      double deltay = dir[s][1]*layerRadius/i;
      x = layerRadius*corner[s][0] + deltax * sr;
      y = layerRadius*corner[s][1] + deltay * sr;
    }

    virtual void computeRadius(int w, int h, double r[2])
    {
      int numLayers = Grid.GetDimensions().first;
      double hexDiameter;
      if(this->Grid.GetGeometrySubType() & ANGLE_60 ||
         this->Grid.GetGeometrySubType() & ANGLE_30)
      {
        if(this->Grid.GetGeometrySubType() & ANGLE_60 && this->Grid.GetGeometrySubType() & FLAT)
        {
          double n = 1/(1.75*numLayers - 0.5);
          hexDiameter = std::min(w * n, h*n/cmbNucMathConst::cos30);

        }
        else if(this->Grid.GetGeometrySubType() & ANGLE_60 &&
                this->Grid.GetGeometrySubType() & VERTEX)
        {
          double n = 2*numLayers - 0.4;
          double nl = 0;
          if(numLayers%2 == 0) nl = numLayers*0.5*1.5 - 0.5;//even
          else nl = (numLayers-1)*0.5*1.5 + 0.5;//odd
          hexDiameter = std::min(w/(cmbNucMathConst::cos30Squared*n),
                                 h/(2*(nl+0.1)*cmbNucMathConst::cos30));
        }
        else
        {
          double n = 1/(1.75*numLayers - 0.5);
          hexDiameter = std::min(w * n, h * (n * 1.8)/cmbNucMathConst::cos30);
        }
      }
      else
      {
        double nl = numLayers*0.75 - 0.25;
        double den[] = { cmbNucMathConst::cos30*4*static_cast<double>(2 * numLayers - 1),
                         static_cast<double>(8*nl) };
        hexDiameter = std::min((w-10)/den[this->Grid.getFullCellMode()],
                               (h-10)/den[(this->Grid.getFullCellMode()+1)%2])*2;
      }
      hexDiameter = std::max(hexDiameter, 20.0); // Enforce minimum size for hexes
      r[0] = hexDiameter / 2.0;
      r[1] = cmbNucMathConst::cos30*r[0]*4;
    }

    double corner[6][2];
    double dir[6][2];
  };

  class convertRect: public cmbNucCordinateConverterInternal
  {
    friend class cmbNucCordinateConverter;
  public:
    convertRect(Lattice & g, bool fi):cmbNucCordinateConverterInternal(g), flip_i(fi)
    {
      assert( this->Grid.GetGeometryType() != HEXAGONAL );
    }

    virtual void convertToPixelXY(int i, int j, double & x, double & y, double r1, double r2)
    {
      std::pair<int, int> wh = this->Grid.GetDimensions();
      r1 *=2; r2 *= 2;
      if(flip_i) i = wh.first-1-i;
      y = (i)*r1;
      x = (j)*r2;
    }

    virtual void computeRadius(int w, int h, double r[2])
    {
      std::pair<int, int> wh = this->Grid.GetDimensions();
      double radius = std::min(w, h)/std::max(wh.first, wh.second)*0.5;
      r[0] = r[1] = std::max(radius, 20.0);
    }
  private:
    bool flip_i;
  };
}

cmbNucCordinateConverter::cmbNucCordinateConverter(Lattice & lat, bool control)
{
  if( lat.GetGeometryType() == HEXAGONAL )
  {
    internal = new convertHex(lat, control);
  }
  else
  {
    internal = new convertRect(lat, !control);
  }
}

cmbNucCordinateConverter::~cmbNucCordinateConverter()
{
  delete internal;
}

void cmbNucCordinateConverter
::convertToPixelXY(int i, int j, double & x, double & y, double radius)
{
  this->convertToPixelXY( i, j, x, y,radius, radius);
}

void cmbNucCordinateConverter::convertToPixelXY(int i, int j, double & x, double & y,
                                                double radius1, double radius2)
{
  internal->convertToPixelXY(i, j, x, y, radius1, radius2);
}

void cmbNucCordinateConverter::computeRadius(int w, int h, double r[2])
{
  internal->computeRadius(w,h,r);
}

void cmbNucCordinateConverter::convertToHexCordinates(size_t level, size_t ringI, int & x, int & y )
{
  if(level == 0)
  {
    x = 0; y = 0; return;
  }
  static int origin[6][2] = {{-1,0},{0,-1},{1,-1},{1,0},{0,1},{-1,1}};
  static int direction[6][2] = {{1,-1},{1,0},{0,1},{-1,1},{-1,0},{0,-1}};
  size_t s = ringI/level;
  int r = static_cast<int>(ringI%level);
  x = level*origin[s][0] + direction[s][0]*r;
  y = level*origin[s][1] + direction[s][1]*r;
}

void cmbNucCordinateConverter
::convertFromHexCordinates(int x, int y, size_t & level, size_t & ringI)
{
  int zyx[] = {-(x + y), y, x};
  level = std::max(std::abs(zyx[0]), std::max(std::abs(zyx[1]),std::abs(zyx[2])));
  if(level == 0)
  {
    ringI = 0;
    return;
  }
  int ring = 0;
  int second = 0;
  static const int signs[] = {1,-1};
  for(int i = 0; i < 6; ++i)
  {
    int at = i%3;
    int sign = signs[(i%2)];
    if(zyx[at] == sign*static_cast<int>(level))
    {
      ring = i;
      second = (at+1)%3;
      break;
    }
  }
  ringI = static_cast<size_t>(level*ring+std::abs(zyx[second]));
}

void cmbNucCordinateConverter::convertFromHexCordToEuclid(int i, int j, double & x, double & y)
{
  x = (i + j*0.5);
  y = cmbNucMathConst::cos30 * j;
}
