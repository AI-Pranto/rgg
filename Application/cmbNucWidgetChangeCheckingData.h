#ifndef cmbNucWidgetChangeCheckingData_h
#define cmbNucWidgetChangeCheckingData_h

#include "cmbNucWidgetChangeChecker.h"

class cmbNucWidgetChangeCheckingData
{
public:
  enum mode{do_nothing, reverted_to_orginal, newly_changed};
  cmbNucWidgetChangeCheckingData(QString ov = QString())
  :original_value(ov), changed(false)
  {}

  void setOriginal(QString ov)
  {
    changed = false;
    original_value = ov;
  }

  mode test(QString v)
  {
    if(v != original_value && !changed)
    {
      changed = true;
      return newly_changed;
    }
    if( v == original_value && changed)
    {
      changed = false;
      return reverted_to_orginal;
    }
    return do_nothing;
  }
protected:
  QString original_value;
  bool changed;
};
#endif
