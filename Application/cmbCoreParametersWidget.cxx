
#include "cmbCoreParametersWidget.h"
#include "ui_qCoreParameters.h"
#include "cmbNucNeumannWidget.h"

#include "cmbNucCore.h"
#include "cmbNucDefaults.h"

#include <QLabel>
#include <QPointer>
#include <QtDebug>
#include <QDebug>
#include <QIntValidator>
#include <QSettings>
#include <QDir>
#include <QFileInfo>
#include <QFileDialog>

#include <sstream>
#include <cmath>

class cmbCoreParametersWidget::cmbCoreParametersWidgetInternal :
  public Ui::qCoreParametersWidget
{
public:
  cmbNucNeumannWidget * neumannWidget;
};

//--------------------------------------------------------------------------------------------------
cmbCoreParametersWidget::cmbCoreParametersWidget(QWidget *p)
  : QFrame(p)
{
  this->Internal = new cmbCoreParametersWidgetInternal;
  this->Internal->setupUi(this);
  this->Core = NULL;
  this->Internal->neumannWidget = new cmbNucNeumannWidget(this);
  this->Internal->neumanSetLayout->addWidget(this->Internal->neumannWidget);
}

//--------------------------------------------------------------------------------------------------
cmbCoreParametersWidget::~cmbCoreParametersWidget()
{
  delete this->Internal->neumannWidget;
  delete this->Internal;
}

//--------------------------------------------------------------------------------------------------

namespace
{
  void corePostApply(cmbNucCore *CoreObj, bool change)
  {
    if(change)
    {
      CoreObj->setAndTestDiffFromFiles(change);
      CoreObj->getConnection()->componentChanged(NULL);
    }
  }
}

void cmbCoreParametersWidget::setCore(cmbNucCore *CoreObj,
                                      cmbNucWidgetChangeChecker & checker)
{
  this->Internal->neumannWidget->set(CoreObj);
  connect( &checker, SIGNAL(sendApply()), this->Internal->neumannWidget, SLOT(apply()),
          Qt::UniqueConnection );
  connect( &checker, SIGNAL(sendReset()), this->Internal->neumannWidget, SLOT(reset()),
          Qt::UniqueConnection );
  connect( &checker, SIGNAL(sendClear()), this->Internal->neumannWidget, SLOT(clear()),
          Qt::UniqueConnection );
  connect( this->Internal->neumannWidget,  SIGNAL(sendMode(cmbNucWidgetChangeChecker::mode)),
           &checker, SLOT(updateMode(cmbNucWidgetChangeChecker::mode)), Qt::UniqueConnection );

  this->Core = CoreObj;
  cmbNucCoreParams * prms = &(CoreObj->getParams());

  typedef cmbNucWidgetChangeChecker::cmbNucWidgetChangeCheckerWrapper WRAPPER;

  //Internal->MergeTolerance  mergetolerance double QLineEdit
#define CHECKERFUN(NAME, DATA_TYPE, WIDGET, W_TYPE, IS_VALID_DATA)                                 \
{                                                                                                  \
  boost::function<DATA_TYPE (void)> getData = boost::bind(&cmbNucCoreParams::get##NAME, prms);     \
  boost::function<void (DATA_TYPE)> setData = boost::bind(&cmbNucCoreParams::set##NAME, prms, _1); \
  boost::function<void (void)> setDataInvalid = boost::bind(&cmbNucCoreParams::clear##NAME, prms); \
  boost::function<bool (void)> dataIsSet = boost::bind(&cmbNucCoreParams::NAME##IsSet, prms);      \
                                                                                                   \
  boost::function<bool (W_TYPE)> isValidData = IS_VALID_DATA;                                      \
                                                                                                   \
  WIDGET * widget = this->Internal->NAME;                                                          \
  boost::function<void (bool)> postApply = boost::bind(&corePostApply, CoreObj, _1);               \
                                                                                                   \
  cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE> * tmpData =                                          \
        new cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE>(widget, getData, setData, setDataInvalid,  \
                                                        dataIsSet, isValidData,  postApply);       \
  checker.registerToTrack(tmpData);                                                                \
}
  CHECKERFUN(MergeTolerance, double, QLineEdit, QString, WRAPPER::isNumber )
  CHECKERFUN(Geometry, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(ProblemType, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(SaveParallel, std::string, QComboBox, QString, WRAPPER::isNotEmtpyString)
  CHECKERFUN(Info, bool, QCheckBox, bool, WRAPPER::alwaysValid<bool> )
  CHECKERFUN(MeshInfo, bool, QCheckBox, bool, WRAPPER::alwaysValid<bool> )
  CHECKERFUN(UnknownKeyWords, QString, QPlainTextEdit, QString, WRAPPER::alwaysValid<QString>)
  CHECKERFUN(MeshOutputFilename, std::string, QLineEdit, QString, WRAPPER::isNotEmtpyString)

#undef CHECKERFUN

  //Extrude
  typedef cmbNucCoreParams::ExtrudeStruct ExtrudeStruct;
  ExtrudeStruct * extd = &(prms->getExtrude());

#define CHECKERFUN(NAME, DATA_TYPE, WIDGET, W_TYPE, IS_VALID_DATA)                                 \
{                                                                                                  \
  boost::function<DATA_TYPE (void)> getData = boost::bind(&ExtrudeStruct::get##NAME, extd);        \
  boost::function<void (DATA_TYPE)> setData = boost::bind(&ExtrudeStruct::set##NAME, extd, _1);    \
  boost::function<void (void)> setDataInvalid = boost::bind(&ExtrudeStruct::clear, extd);          \
  boost::function<bool (void)> dataIsSet = boost::bind(&ExtrudeStruct::isValid, extd);             \
                                                                                                   \
  boost::function<bool (W_TYPE)> isValidData = IS_VALID_DATA;                                      \
                                                                                                   \
  WIDGET * widget = this->Internal->Extrude##NAME;                                                 \
  boost::function<void (bool)> postApply = boost::bind(&corePostApply, CoreObj, _1);               \
                                                                                                   \
  cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE> * tmpData =                                          \
        new cmbNucGetSetData<WIDGET, W_TYPE, DATA_TYPE>(widget, getData, setData, setDataInvalid,  \
                                                        dataIsSet, isValidData,  postApply);       \
  checker.registerToTrack(tmpData);                                                                \
}
  CHECKERFUN(Size, double, QLineEdit, QString, WRAPPER::isNumber )
  CHECKERFUN(Divisions,int, QLineEdit, QString, WRAPPER::isNumber )

#undef CHECKERFUN
}
