#ifndef cmbNucPartLibrary_H
#define cmbNucPartLibrary_H

#include <string>
#include <vector>
#include <map>
#include <QColor>
#include <QObject>
#include <QPointer>
#include <QStringList>

#include "cmbNucPart.h"

class cmbNucPartLibrary;

class cmbNucPartLibraryConnection: public QObject
{
  Q_OBJECT
  friend class cmbNucPartLibrary;
public:
  cmbNucPartLibraryConnection(cmbNucPartLibrary * part);
signals:
  void partChanged(cmbNucPart *);
private slots:
  void partDeleted(cmbNucPart*);
private:
  cmbNucPartLibrary * v;
};

class cmbNucPartLibrary
{
public:
  friend class cmbNucPartLibraryConnection;
  cmbNucPartLibrary();
  virtual ~cmbNucPartLibrary();

  //takes ownership when successful. Returns true if successful.
  bool addPart(cmbNucPart * part);
  //Adds a duct if it is unique.  if not returns the previous added duct
  bool addPart(cmbNucPart ** part);
  bool addPart(cmbNucPart * part,
               std::map<QString, QString> & nameChange,
               std::map<QString, QString> & labelChange );
  void removePart(cmbNucPart* part);
  cmbNucPart* GetPartByName(const QString &name);
  cmbNucPart* GetPartByLabel(const QString &label);
  cmbNucPart* GetPart(int pc) const;
  enum enumGetPartMode{BY_NAME, BY_LABEL};
  template<class RTYPE> RTYPE* GetPart(const QString &key, enumGetPartMode mode)
  {
    switch(mode)
    {
      case BY_NAME: return dynamic_cast<RTYPE *>(GetPartByName(key));
      case BY_LABEL: return dynamic_cast<RTYPE *>(GetPartByLabel(key));
    }
    return NULL;
  }
  template<class RTYPE> RTYPE* GetPart(int pc) const
  {
    return dynamic_cast<RTYPE*>(this->GetPart(pc));
  }
  std::size_t GetNumberOfParts() const;
  void fillListJustName(std::vector< std::pair<QString, cmbNucPart *> > & l);
  void fillListNamePlusLabel(std::vector< std::pair<QString, cmbNucPart *> > & l);
  bool nameConflicts(QString n) const;
  bool labelConflicts(QString l) const;
  bool replaceName(QString oldN, QString newN);
  bool replaceLabel(QString oldL, QString newL);
  QString extractLabel(QString const& el) const;

  cmbNucPartLibrary * clone();

  void removeFakeBoundaryLayer(QString blname);

  cmbNucPart * getEquivelent(cmbNucPart * pc) const;

  cmbNucPartLibraryConnection * getConnection()
  { return Connection; }

protected:
  std::vector<cmbNucPart*> Parts;
  std::map<QString, cmbNucPart*> NameToPart;
  std::map<QString, cmbNucPart*> LabelToPart;
  cmbNucPartLibraryConnection * Connection;
  void remove(cmbNucPart * part);
};

#endif
