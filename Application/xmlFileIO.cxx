#include "xmlFileIO.h"

#include "cmbNucCore.h"
#include "cmbNucAssembly.h"
#include "cmbNucMaterialColors.h"
#include "cmbNucPartLibrary.h"
#include "cmbNucDefaults.h"
#include "cmbNucAssemblyLink.h"

#define PUGIXML_HEADER_ONLY
#include "src/pugixml.cpp"

#include <QPointer>
#include <QString>
#include <QColor>
#include <QFileInfo>
#include <QMessageBox>
#include <QDir>
#include <QDebug>

#include <sstream>

#include <boost/bind.hpp>

//TAGS
namespace
{
  const std::string CORE_TAG = "NuclearCore";
  const std::string MATERIALS_TAG = "Materials";
  const std::string MATERIAL_TAG = "Material";
  const std::string NAME_TAG = "Name";
  const std::string LABEL_TAG = "Label";
  const std::string COLOR_TAG = "Color";
  const std::string DUCT_CELL_TAG = "DuctCell";
  const std::string DUCT_LAYER_TAG = "DuctLayer";
  const std::string LEGEND_COLOR_TAG = "LegendColor";
  const std::string LOC_TAG = "Loc";
  const std::string THICKNESS_TAG = "Thickness";
  const std::string MATERIAL_LAYER_TAG = "MaterialLayer";
  const std::string CYLINDER_TAG = "Cylinder";
  const std::string FRUSTRUM_TAG = "Frustrum";
  const std::string RADIUS_TAG = "Radius";
  const std::string TYPE_TAG = "Type";
  const std::string SUB_TYPE_TAG = "SubType";
  const std::string GRID_TAG = "Grid";
  const std::string DUCT_TAG = "Duct";
  const std::string GEOMETRY_TAG = "Geometry";
  const std::string CENTER_PINS_TAG = "CenterPins";
  const std::string PITCH_TAG = "Pitch";
  const std::string VALUE_TAG = "Value";
  const std::string AXIS_TAG = "Axis";
  const std::string DIRECTION_TAG = "Direction";
  const std::string PARAMETERS_TAG = "Parameters";
  const std::string MOVE_TAG = "Move";
  const std::string CENTER_TAG = "Center";
  const std::string UNKNOWN_TAG = "Unknown";
  const std::string DUCTS_TAG = "Ducts";
  const std::string PINS_TAG = "Pins";
  const std::string DEFAULTS_TAG = "Defaults";
  const std::string PINCELL_TAG = "PinCell";
  const std::string NEUMANN_VALUE_TAG = "NeumannValue";
  const std::string LENGTH_TAG = "Length";
  const std::string Z0_TAG = "Z0";
  const std::string STR_TAG = "Str";
  const std::string LATTICE_TAG = "Lattice";
  const std::string SIDE_TAG = "Side";
  const std::string ID_TAG = "Id";
  const std::string EQUATION_TAG = "Equation";
  const std::string SIZE_TAG = "Size";
  const std::string DIVISIONS_TAG = "Divisions";
  const std::string AXIAL_MESH_SIZE_TAG = "AxialMeshSize";
  const std::string EDGE_INTERVAL_TAG = "EdgeInterval";
  const std::string MESH_TYPE_TAG = "MeshType";
  const std::string ASSEMBLY_TAG = "Assembly";
  const std::string BACKGROUND_TAG = "Background";
  const std::string MODE_TAG = "Mode";
  const std::string CYLINDER_RADIUS_TAG = "CylinderRadius";
  const std::string CYLINDER_OUTER_SPACING_TAG = "CylinderOuterSpacing";
  const std::string BACKGROUND_FILENAME_TAG = "BackgroundFileName";
  const std::string MESH_FILENAME_TAG = "MeshFileName";
  const std::string ROTATE_TAG = "Rotate";
  const std::string ASSEMBLY_LINK_TAG = "AssemblyAlternative";
  const std::string BOUNDARY_LAYER_TAG = "BoundaryLayer";
  const std::string BIAS_TAG = "Bias";
  const std::string INTERVAL_TAG = "Interval";
  const std::string BLANK_TAG = "Blank";
}

class xmlHelperClass
{
public:
  bool openReadFile(std::string fname, pugi::xml_document & document);
  bool writeToString(std::string & out, cmbNucCore & core);
  bool writeStringToFile(std::string fname, std::string & out);

  bool writeAssyPart(pugi::xml_node & node, cmbNucPart * apart);
  bool write(pugi::xml_node & node, cmbNucAssembly * assy);

  bool write(pugi::xml_node & materialElement, cmbNucMaterialColors * materials);
  bool write(pugi::xml_node & materialElement, QPointer< cmbNucMaterial > material);

  template<class PART_TYPE> bool write(pugi::xml_node & node, cmbNucPartLibrary * dl);

  bool write(pugi::xml_node & node, DuctCell * dc);
  bool write(pugi::xml_node & node, Duct * dc);

  bool write(pugi::xml_node & node, PinCell * dc);
  bool write(pugi::xml_node & node, Cylinder * dc);
  bool write(pugi::xml_node & node, Frustum * dc);
  bool writePSP(pugi::xml_node & node, PinSubPart * dc);

  bool writeCorePart(pugi::xml_node & node, cmbNucPart * link);
  bool write(pugi::xml_node & node, cmbNucAssemblyLink * link);

  bool write(pugi::xml_node & node, Lattice & lattice);

  bool write(pugi::xml_node & node, cmbNucMaterialLayer const& v);

  bool write(pugi::xml_node & node, std::string attName,
             std::vector<cmbNucCoreParams::NeumannSetStruct> const&);

  bool write(pugi::xml_node & node, std::string attName, cmbNucCoreParams::ExtrudeStruct const&);

  bool write(pugi::xml_node & node, cmbNucCore::boundaryLayer * bl);

  bool write(pugi::xml_node & node, std::string attName, QString const& v)
  {
    return write(node, attName, v.toStdString());
  }
  bool write(pugi::xml_node & node, std::string attName, std::string const& v)
  {
    node.append_attribute(attName.c_str()).set_value(v.c_str());
    return true;
  }
  bool write(pugi::xml_node & node, std::string attName, QColor const& v)
  {
    QString str = QString("%1, %2, %3, %4").arg(v.redF()).arg(v.greenF()).arg(v.blueF()).arg(v.alphaF());
    return write(node, attName, str);
  }
  bool write(pugi::xml_node & node, std::string attName, double const& v)
  {
    QString str = QString("%1").arg(v, 0, 'g', 9);
    return write(node, attName, str);
  }
  bool write(pugi::xml_node & node, std::string attName, int const& v)
  {
    QString str = QString("%1").arg(v);
    return write(node, attName, str);
  }
  bool write(pugi::xml_node & node, std::string attName, unsigned int const& v)
  {
    QString str = QString("%1").arg(v);
    return write(node, attName, str);
  }
  bool write(pugi::xml_node & node, std::string attName, bool const& v)
  {
    QString str = QString("%1").arg(v);
    return write(node, attName, str);
  }
  bool write(pugi::xml_node & node, std::string attName, double const* v, int size)
  {
    QString str;
    switch(size)
    {
      case 1: return write(node, attName, *v);
      case 2: str = QString("%1, %2").arg(v[0], 0, 'g', 9).arg(v[1], 0, 'g', 9); break;
      case 3: str = QString("%1, %2, %3").arg(v[0]).arg(v[1], 0, 'g', 9).arg(v[2], 0, 'g', 9); break;
      case 4: str = QString("%1, %2, %3, %4").arg(v[0], 0, 'g', 9).arg(v[1], 0, 'g', 9).arg(v[2], 0, 'g', 9).arg(v[3], 0, 'g', 9); break;
    }
    return write(node, attName, str);
  }

  bool read(std::string const& in, cmbNucCore & core);
  bool read(pugi::xml_node & node, cmbNucMaterialColors * materials);
  bool read(pugi::xml_node & node, QString & name, QString & label, QColor & color);

  template <class TYPE> bool read(pugi::xml_node & node, cmbNucPartLibrary * dl,
                                  cmbNucMaterialColors * materials);

  bool read(pugi::xml_node & node, PinCell * dc, cmbNucMaterialColors * materials);
  bool read(pugi::xml_node & node, Cylinder * dc, cmbNucMaterialColors * materials);
  bool read(pugi::xml_node & node, Frustum * dc, cmbNucMaterialColors * materials);
  bool readPSP(pugi::xml_node & node, PinSubPart * dc, cmbNucMaterialColors * materials);

  bool read(pugi::xml_node & node, cmbNucMaterialLayer * ml, cmbNucMaterialColors * materials);

  bool read(pugi::xml_node & node, DuctCell * dc, cmbNucMaterialColors * materials);
  bool read(pugi::xml_node & node, Duct * dc, cmbNucMaterialColors * materials);

  bool read(pugi::xml_node & node, cmbNucAssembly* assy);
  bool readAssyPart(pugi::xml_node & node, cmbNucPart* assy);

  bool read(pugi::xml_node & node, cmbNucAssemblyLink * link, cmbNucCore & core);

  bool read(pugi::xml_node & node, cmbNucCore::boundaryLayer * bl,
            cmbNucMaterialColors * materials);

  bool readLattice(pugi::xml_node & node, LatticeContainer & container);

  bool read(pugi::xml_node & node, std::string attName,
            std::vector<cmbNucCoreParams::NeumannSetStruct> &);

  bool read(pugi::xml_node & node, std::string attName, cmbNucCoreParams::ExtrudeStruct &);

  bool read(pugi::xml_node & node, std::string attName, QColor & v)
  {
    pugi::xml_attribute att = node.attribute(attName.c_str());
    if(!att) return false;
    QString ts(att.value());
    QStringList list1 = ts.split(",");
    v = QColor::fromRgbF(list1.value(0).toDouble(),
                         list1.value(1).toDouble(),
                         list1.value(2).toDouble(),
                         list1.value(3).toDouble());
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, std::string & v)
  {
    pugi::xml_attribute att = node.attribute(attName.c_str());
    if(!att) return false;
    v = std::string(att.value());
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, QString & v)
  {
    pugi::xml_attribute att = node.attribute(attName.c_str());
    if(!att) return false;
    v = QString(att.value());
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, double & v)
  {
    QString str;
    if(!read(node, attName, str)) return false;
    v = str.toDouble();
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, int & v)
  {
    QString str;
    if(!read(node, attName, str)) return false;
    v = str.toInt();
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, unsigned int & v)
  {
    QString str;
    if(!read(node, attName, str)) return false;
    v = str.toUInt();
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, bool & v)
  {
    QString str;
    if(!read(node, attName, str)) return false;
    v = static_cast<bool>(str.toInt());
    return true;
  }

  bool read(pugi::xml_node & node, std::string attName, double * v, int size)
  {
    QString str;
    if(!read(node, attName, str)) return false;
    QStringList list1 = str.split(",");
    for(int at = 0; at < size; ++at)
    {
      v[at] = list1.value(at).toDouble();
    }
    return true;
  }
};

bool xmlHelperClass::openReadFile(std::string fname, pugi::xml_document & document)
{
  std::ifstream in(fname.c_str(), std::ios::in);
  if (!in)
  {
    return false;
  }

  // Allocate string
  std::string content;
  in.seekg(0, std::ios::end);
  content.resize(in.tellg());

  in.seekg(0, std::ios::beg);
  in.read(&content[0], content.size());
  in.close();

  pugi::xml_parse_result presult = document.load_buffer(content.c_str(), content.size());
  if (presult.status != pugi::status_ok)
  {
    return false;
  }
  return true;
}

bool xmlHelperClass::write(pugi::xml_node & materialElement,
                           cmbNucMaterialColors * cnmc)
{
  std::vector< QPointer< cmbNucMaterial > > materials = cnmc->getMaterials();
  for(size_t i = 0; i < materials.size(); ++i)
  {
    pugi::xml_node mElement = materialElement.append_child(MATERIAL_TAG.c_str());
    this->write(mElement, materials[i]);
  }
  return true;
}

bool xmlHelperClass::write(pugi::xml_node & materialElement, QPointer< cmbNucMaterial > material)
{
  bool r = true;
  r &= write(materialElement, NAME_TAG.c_str(), material->getName());
  r &= write(materialElement, LABEL_TAG.c_str(), material->getLabel());
  r &= write(materialElement, COLOR_TAG.c_str(), material->getColor());
  return r;
}

namespace
{

std::string getTag(DuctCell*)
{
  return DUCT_CELL_TAG;
}

std::string getTag(PinCell*)
{
  return PINCELL_TAG;
}

}

template<class PART_TYPE>
bool xmlHelperClass::write(pugi::xml_node & node, cmbNucPartLibrary * dl)
{
  std::size_t num = dl->GetNumberOfParts();
  bool r = true;
  for(size_t i = 0; i < num; ++i)
  {
    PART_TYPE * part = dl->GetPart<PART_TYPE>(static_cast<int>(i));
    pugi::xml_node xn = node.append_child(getTag(part).c_str());
    r &= this->write(xn, part);
  }
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, DuctCell * dc)
{
  bool r = true;
  r &= write(node, NAME_TAG.c_str(), dc->getName());
  size_t num = dc->numberOfDucts();
  for(size_t i = 0; i < num; ++i)
  {
    pugi::xml_node xn = node.append_child(DUCT_LAYER_TAG.c_str());
    r &= this->write(xn, dc->getDuct(static_cast<int>(i)));
  }
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, Duct * dc)
{
  bool r = true;
  r &= write(node, LOC_TAG.c_str(),
             QString("%1, %2, %3, %4").arg(dc->getX(), 0, 'g', 9)
                                      .arg(dc->getY(), 0, 'g', 9)
                                      .arg(dc->getZ1(), 0, 'g', 9)
                                      .arg(dc->getZ2(), 0, 'g', 9));
  double thickness[] = { dc->getThickness(0), dc->getThickness(1) };
  r &= write(node, THICKNESS_TAG.c_str(), thickness, 2);

  size_t num = dc->NumberOfLayers();
  for(size_t i = 0; i < num; ++i)
  {
    pugi::xml_node xn = node.append_child(MATERIAL_LAYER_TAG.c_str());
    r &= this->write(xn, dc->getMaterialLayer(static_cast<int>(i)));
  }
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, PinCell * dc)
{
  bool r = writeAssyPart(node, dc);
  if(dc->cellMaterialSet())
    r &= write(node, MATERIAL_TAG.c_str(), dc->getCellMaterial()->getName());

  for(unsigned int i = 0; i < dc->GetNumberOfParts(); ++i)
  {
    PinSubPart * part = dc->GetPart(i);
    if(part->GetType() == PinSubPart::CYLINDER)
    {
      pugi::xml_node xn = node.append_child(CYLINDER_TAG.c_str());
      r &= this->write(xn, dynamic_cast<Cylinder*>(part));
    }
    else
    {
      pugi::xml_node xn = node.append_child(FRUSTRUM_TAG.c_str());
      r &= this->write(xn, dynamic_cast<Frustum*>(part));
    }
  }

  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, Cylinder * c)
{
  bool r = true;
  r &= write(node, RADIUS_TAG.c_str(), c->r);
  r &= writePSP(node, c);
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, Frustum * f)
{
  bool r = true;
  r &= write(node, RADIUS_TAG.c_str(), f->r, 2);
  r &= writePSP(node, f);
  return r;
}

bool xmlHelperClass::writePSP(pugi::xml_node & node, PinSubPart * p)
{
  bool r = true;
  r &= write(node, LOC_TAG.c_str(), QString("%1, %2, %3, %4").arg(p->x, 0, 'g', 9).arg(p->y, 0, 'g', 9).arg(p->getZ1()).arg(p->getZ2(), 0, 'g', 9));
  size_t num = p->GetNumberOfLayers();
  for(size_t i = 0; i < num; ++i)
  {
    pugi::xml_node xn = node.append_child(MATERIAL_LAYER_TAG.c_str());
    r &= this->write(xn, p->getMaterialLayer(static_cast<int>(i)));
  }
  return true;
}

bool xmlHelperClass::write(pugi::xml_node & node, cmbNucMaterialLayer const& v)
{
  bool r = true;
  r &= write(node, THICKNESS_TAG.c_str(), v.getThickness(), 2);
  r &= write(node, MATERIAL_TAG.c_str(), v.getMaterial()->getName());
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, cmbNucCore::boundaryLayer * bl)
{
  bool r = true;
  r &= write(node, MATERIAL_TAG.c_str(), bl->interface_material->getName());
  r &= write(node, THICKNESS_TAG.c_str(), bl->Thickness);
  r &= write(node, BIAS_TAG.c_str(), bl->Bias);
  r &= write(node, INTERVAL_TAG.c_str(), bl->Intervals);
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, Lattice & lattice)
{
  bool r = true;
  r &= write(node, TYPE_TAG.c_str(), static_cast<unsigned int>(lattice.GetGeometryType()));
  r &= write(node, SUB_TYPE_TAG.c_str(), lattice.GetGeometrySubType());
  QString grid;
  for(unsigned int i = 0; i < lattice.getSize(); ++i)
  {
    for(unsigned int j = 0; j < lattice.getSize(i); ++j)
    {
      if(j != 0) grid += ",";
      Lattice::Cell cell = lattice.GetCell(i, j);
      grid += cell.getLabel();
    }
    grid += ";";
  }
  r &= write(node, GRID_TAG.c_str(), grid);
  return r;
}

bool xmlHelperClass::writeAssyPart(pugi::xml_node & node, cmbNucPart * part)
{
  bool r = write(node, NAME_TAG.c_str(), part->getName());
  r &= write(node, LABEL_TAG.c_str(), part->getLabel());
  r &= write(node, LEGEND_COLOR_TAG.c_str(), part->GetLegendColor());
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, cmbNucAssembly * assy)
{
  bool r = writeAssyPart(node, assy);
  r &= write(node, DUCT_TAG.c_str(), assy->getAssyDuct().getName());
  r &= write(node, GEOMETRY_TAG.c_str(), assy->getGeometryLabel());
  r &= write(node, CENTER_PINS_TAG.c_str(), assy->isPinsAutoCentered());
  r &= write(node, PITCH_TAG.c_str(),
             QString("%1, %2").arg(assy->getPitchX(), 0, 'g', 9).arg(assy->getPitchY(), 0, 'g', 9));
  r &= write(node, ROTATE_TAG.c_str(), assy->getZAxisRotation());

  pugi::xml_node paramNode = node.append_child(PARAMETERS_TAG.c_str());
  {
    cmbAssyParameters * params = assy->GetParameters();

    //Other Parameters
    if(params->isSetMoveXYZ())
    {
      r &= write(paramNode, MOVE_TAG.c_str(), params->getMoveXYZ(), 3);
    }
    if(params->isSetCenter())
      r &= write(paramNode, CENTER_TAG.c_str(), params->getCenter());

    if(params->isSetSave_Exodus())
    {
      r &= write(paramNode, "save_exodus", params->getSave_Exodus());
    }
#define FUN_SIMPLE(TYPE,X,Var,Key)\
    if(params->isSet##Var())\
    {\
      r &= write(paramNode, #Key, params->get##Var());\
    }
    ASSYGEN_EXTRA_VARABLE_MACRO()
    FUN_SIMPLE(double,      QString, AxialMeshSize,            AxialMeshSize)
#undef FUN_SIMPLE

    r &= write(node, "materialset_startid", params->getMaterialSet_StartId());
    r &= write(node, "neumannset_startid", params->getNeumannSet_StartId());

    std::stringstream ss(params->getUnknownParams().toStdString().c_str());
    std::string line;
    while( std::getline(ss, line))
    {
      pugi::xml_node tn = node.append_child(UNKNOWN_TAG.c_str());
      if(!write(tn, STR_TAG.c_str(), line)) return false;
    }
  }

  pugi::xml_node lnode = node.append_child(LATTICE_TAG.c_str());
  r &= write( lnode, assy->getLattice());

  return r;
}

bool xmlHelperClass::writeCorePart(pugi::xml_node & node, cmbNucPart * part)
{
  if( part->GetType() == CMBNUC_ASSEMBLY_LINK )
    return write(node, dynamic_cast<cmbNucAssemblyLink*>(part));
  if( part->GetType() == CMBNUC_ASSEMBLY )
    return write(node, dynamic_cast<cmbNucAssembly*>(part));
  return writeAssyPart(node, part);
}

bool xmlHelperClass::write(pugi::xml_node & node, cmbNucAssemblyLink * link)
{
  bool r = writeAssyPart(node, link);
  r &= write(node, ASSEMBLY_TAG.c_str(), link->getLink()->getLabel());
  r &= write(node, "MaterialStartID", link->getMaterialStartID());
  r &= write(node, "NeumannStartID", link->getNeumannStartID());
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, std::string attName,
                           std::vector<cmbNucCoreParams::NeumannSetStruct> const& nssv)
{
  bool r = true;
  pugi::xml_node ttnode = node.append_child(attName.c_str());
  for(size_t i = 0; i < nssv.size(); ++i)
  {
    pugi::xml_node nssnode = ttnode.append_child(NEUMANN_VALUE_TAG.c_str());
    cmbNucCoreParams::NeumannSetStruct const& nss = nssv[i];
    r &= write(nssnode, SIDE_TAG.c_str(), nss.Side);
    r &= write(nssnode, ID_TAG.c_str(), nss.Id);
    r &= write(nssnode, EQUATION_TAG.c_str(), nss.Equation);
  }
  return r;
}

bool xmlHelperClass::write(pugi::xml_node & node, std::string attName,
                           cmbNucCoreParams::ExtrudeStruct const& es)
{
  bool r = true;
  pugi::xml_node ttnode = node.append_child(attName.c_str());
  r &= write(ttnode, SIZE_TAG.c_str(), es.getSize());
  r &= write(ttnode, DIVISIONS_TAG.c_str(), es.getDivisions());
  return r;
}

bool xmlHelperClass::writeToString(std::string & out, cmbNucCore & core)
{
  pugi::xml_document document;
  pugi::xml_node rootElement = document.append_child(CORE_TAG.c_str());
  pugi::xml_node mnode = rootElement.append_child(MATERIALS_TAG.c_str());
  if(!write(mnode, cmbNucMaterialColors::instance())) return false;
  pugi::xml_node dnode = rootElement.append_child(DUCTS_TAG.c_str());
  if(!write<DuctCell>(dnode, core.getDuctLibrary())) return false;

  pugi::xml_node pnode = rootElement.append_child(PINS_TAG.c_str());
  if(!write<PinCell>(pnode, core.getPinLibrary())) return false;

  //Write defaults
  {
    QPointer<cmbNucDefaults> defaults = core.GetDefaults();
    pugi::xml_node node = rootElement.append_child(DEFAULTS_TAG.c_str());

    double DuctThick[] = {defaults->getDuctThickX(), defaults->getDuctThickY()};
    double length = defaults->getHeight(), z0 = defaults->getZ0();

    if(!write(node, LENGTH_TAG.c_str(), length)) return false;
    if(!write(node, Z0_TAG.c_str(), z0)) return false;
    if(!write(node, THICKNESS_TAG.c_str(), DuctThick, 2)) return false;

    if(defaults->hasAxialMeshSize())
    {
      if(!write( node, AXIAL_MESH_SIZE_TAG.c_str(), defaults->getAxialMeshSize())) return false;
    }
    if(defaults->hasEdgeInterval())
    {
      if(!write( node, EDGE_INTERVAL_TAG.c_str(), defaults->getEdgeInterval())) return false;
    }
    if(defaults->hasMeshType())
    {
      if(!write( node, MESH_TYPE_TAG.c_str(), defaults->getMeshType())) return false;
    }
  }

  int num = core.getNumberOfParts();
  static const std::string part_tags[] = {BLANK_TAG, ASSEMBLY_TAG, ASSEMBLY_LINK_TAG};

  for(int i = 0; i < num; ++i)
  {
    cmbNucPart* part = core.getPart(i);
    pugi::xml_node node = rootElement.append_child(part_tags[part->GetType()].c_str());
    if(!writeCorePart(node, part)) return false;
  }

  //write the background
  {
    cmbNucCoreParams::ReactorVessel & vessel = core.getVessel();
    pugi::xml_node node = rootElement.append_child(BACKGROUND_TAG.c_str());
    if(!node) return false;
    if(!write(node, MODE_TAG.c_str(), static_cast<unsigned int>(vessel.getMode()))) return false;
    if(!write(node, CYLINDER_RADIUS_TAG.c_str(), vessel.getCylinderRadius())) return false;
    if(!write(node, CYLINDER_OUTER_SPACING_TAG.c_str(), vessel.getCylinderOuterSpacing()))
      return false;
    if(!write(node, BACKGROUND_FILENAME_TAG.c_str(), vessel.Background)) return false;
  }

  //Write parameters
  {
    pugi::xml_node node = rootElement.append_child(PARAMETERS_TAG.c_str());
    if(!write(node, MESH_FILENAME_TAG.c_str(), core.getMeshOutputFilename())) return false;
#define FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, MSG) \
    if( core.getParams().Var##IsSet() ) \
    {\
      if(!write(node, #Key, core.getParams().get##Var())) return false; \
    }
#define FUN_STRUCT(TYPE,X,Var,Key,DEFAULT, MSG) FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, MSG)

    EXTRA_VARABLE_MACRO()
#undef FUN_SIMPLE
#undef FUN_STRUCT
    if(core.getParams().getExtrude().isValid())
    {
      cmbNucCoreParams::ExtrudeStruct & extrude = core.getParams().getExtrude();
      if(!write(node, "extrude", extrude)) return false;
    }

    std::stringstream ss(core.getParams().getUnknownKeyWords().toStdString().c_str());
    std::string line;
    while( std::getline(ss, line))
    {
      pugi::xml_node tn = node.append_child(UNKNOWN_TAG.c_str());
      if(!write(tn, STR_TAG.c_str(), line)) return false;
    }
  }

  pugi::xml_node lnode = rootElement.append_child(LATTICE_TAG.c_str());
  if(!write(lnode, core.getLattice())) return false;

  //write boundary layers
  for(int i = 0; i < core.getNumberOfBoundaryLayers(); ++i)
  {
    cmbNucCore::boundaryLayer* bl = core.getBoundaryLayer(i);
    pugi::xml_node tnode = rootElement.append_child(BOUNDARY_LAYER_TAG.c_str());
    if(!write(tnode, bl))
    {
      return false;
    }
  }

  std::stringstream oss;
  unsigned int flags = pugi::format_indent;
  document.save(oss, "  ", flags);
  out = oss.str();

  return true;
}

bool xmlHelperClass::writeStringToFile(std::string fname, std::string & out)
{
  std::ofstream outfile;
  outfile.open(fname.c_str());
  if(!outfile) return false;
  outfile << out;
  outfile.close();
  return true;
}

///////////////////////////////////Read//////////////////////////////////////////////////////
bool xmlHelperClass::read(std::string const& in, cmbNucCore & core)
{
  pugi::xml_document document;
  pugi::xml_parse_result presult = document.load_buffer(in.c_str(), in.size());
  if (presult.status != pugi::status_ok)
  {
    return false;
  }
  pugi::xml_node rootElement = document.child(CORE_TAG.c_str());

  //Read material
  {
    pugi::xml_node node = rootElement.child(MATERIALS_TAG.c_str());
    if(!read(node, cmbNucMaterialColors::instance())) return false;
  }
  {
    pugi::xml_node node = rootElement.child(DUCTS_TAG.c_str());
    if(!read<DuctCell>(node, core.getDuctLibrary(), cmbNucMaterialColors::instance())) return false;
  }
  {
    pugi::xml_node node = rootElement.child(PINS_TAG.c_str());
    if(!read<PinCell>(node, core.getPinLibrary(), cmbNucMaterialColors::instance())) return false;
  }

  {
    cmbNucCoreParams::ReactorVessel & vessel = core.getVessel();
    pugi::xml_node node = rootElement.child(BACKGROUND_TAG.c_str());
    unsigned int mode;
    if(!read(node, MODE_TAG.c_str(), mode)) return false;
    double r;
    if(!read(node, CYLINDER_RADIUS_TAG.c_str(), r)) return false;
    int s;
    if(!read(node, CYLINDER_OUTER_SPACING_TAG.c_str(), s)) return false;
    if(!read(node, BACKGROUND_FILENAME_TAG.c_str(), vessel.Background)) return false;
    switch( mode )
    {
      case 0:
        vessel.setMode(cmbNucCoreParams::ReactorVessel::None);
        vessel.Background = "";
        break;
      case 1:
      {
        vessel.setMode(cmbNucCoreParams::ReactorVessel::External);
        //check to make sure the file exists.
        QFileInfo tmpFI( QFileInfo(core.getFileName().c_str()).dir(),
                         vessel.Background.c_str() );
        //qDebug() << tmpFI;
        if(!tmpFI.exists())
        {
          vessel.setMode(cmbNucCoreParams::ReactorVessel::None);
          QMessageBox msgBox;
          msgBox.setText( QString(vessel.Background.c_str()) +
                         QString(" was not found in same director as the core inp file.  Will be"
                                 " ignored."));
          msgBox.exec();
        }
        vessel.BackgroundFullPath = tmpFI.absoluteFilePath().toStdString();
        break;
      }
      case 2:
        vessel.setMode(cmbNucCoreParams::ReactorVessel::Generate);
        core.drawCylinder(r, s);  //TODO Fix this
    }
  }

  //read defaults
  {
    core.initDefaults();
    QPointer<cmbNucDefaults> defaults = core.GetDefaults();
    pugi::xml_node node = rootElement.child(DEFAULTS_TAG.c_str());

    double DuctThick[2];
    double length;
    double z0;

    if(!read(node, LENGTH_TAG.c_str(), length)) return false;
    if(!read(node, Z0_TAG.c_str(),z0))
    {
      z0 = 0;
    }
    if(!read(node, THICKNESS_TAG.c_str(), DuctThick, 2)) return false;

    defaults->setHeight(length);
    defaults->setDuctThickness(DuctThick[0], DuctThick[1]);
    defaults->setZ0(z0);

    double vd;
    int vi;
    QString vs;
    if(read(node, AXIAL_MESH_SIZE_TAG.c_str(), vd))
    {
      defaults->setAxialMeshSize(vd);
    }
    if(read(node, EDGE_INTERVAL_TAG.c_str(), vi))
    {
      defaults->setEdgeInterval(vi);
    }
    if(read( node, MESH_TYPE_TAG.c_str(), vs))
    {
      defaults->setMeshType(vs);
    }
    core.sendDefaults();
  }

  //read assemblies
  for(pugi::xml_node tnode = rootElement.child(ASSEMBLY_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(ASSEMBLY_TAG.c_str()))
  {
    cmbNucAssembly* assy = new cmbNucAssembly("", "", Qt::white);
    core.addPart(assy);
    if(!read(tnode, assy)) return false;
  }

  for(pugi::xml_node tnode = rootElement.child(ASSEMBLY_LINK_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(ASSEMBLY_LINK_TAG.c_str()))
  {
    cmbNucAssemblyLink* al = new cmbNucAssemblyLink;
    if(!read(tnode, al, core))
    {
      return false;
    }
    core.addPart(al);
  }

  for(pugi::xml_node tnode = rootElement.child(BLANK_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(BLANK_TAG.c_str()))
  {
    cmbNucPart* al = new cmbNucPart;
    if(!readAssyPart(tnode, al))
    {
      return false;
    }
    core.addPart(al);
  }

  //Read parameters
  {
    pugi::xml_node node = rootElement.child(PARAMETERS_TAG.c_str());
    {
      std::string outf;
      read(node, MESH_FILENAME_TAG.c_str(), outf);
      core.setMeshOutputFilename(outf);
    }
#define FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, MSG) \
    {\
      TYPE val; \
      if(read(node, #Key, val)) core.getParams().set##Var(val);\
    }
#define FUN_STRUCT(TYPE,X,Var,Key,DEFAULT, MSG) FUN_SIMPLE(TYPE,X,Var,Key,DEFAULT, MSG)
    EXTRA_VARABLE_MACRO()
#undef FUN_SIMPLE
#undef FUN_STRUCT
    {
      cmbNucCoreParams::ExtrudeStruct & extrude = core.getParams().getExtrude();
      read(node, "extrude", extrude);
    }
    QString unknown = core.getParams().getUnknownKeyWords();
    for(pugi::xml_node tnode = node.child(UNKNOWN_TAG.c_str()); tnode;
        tnode = tnode.next_sibling(UNKNOWN_TAG.c_str()))
    {
      std::string tmp;
      if(read(tnode, STR_TAG.c_str(), tmp))
        unknown.append((tmp + "\n").c_str());
    }
    core.getParams().setUnknownKeyWords(unknown);
  }

  pugi::xml_node lnode = rootElement.child(LATTICE_TAG.c_str());
  if(!readLattice(lnode, core)) return false;

  if(core.IsHexType())
  {
    core.getLattice().setFullCellMode(Lattice::HEX_FULL);
    for(size_t i = 0; i < core.getNumberOfParts(); ++i)
    {
      cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly*>(core.getPart(i));
      if(assy == NULL) continue;
      if( core.getLattice().GetGeometrySubType() & ANGLE_60 &&
          core.getLattice().GetGeometrySubType() & VERTEX )
      {
        assy->getLattice().setFullCellMode(Lattice::HEX_FULL);
      }
      else
      {
        assy->getLattice().setFullCellMode(Lattice::HEX_FULL_30);
      }
    }
  }

  //read boundary layers
  for(pugi::xml_node tnode = rootElement.child(BOUNDARY_LAYER_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(BOUNDARY_LAYER_TAG.c_str()))
  {
    cmbNucCore::boundaryLayer* bl = new cmbNucCore::boundaryLayer();
    if(!read(tnode, bl, cmbNucMaterialColors::instance()))
    {
      return false;
    }
    core.addBoundaryLayer(bl);
  }

  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, cmbNucMaterialColors * materials)
{
  if(materials == NULL) return false;
  QString name, label;
  QColor color;
  for (pugi::xml_node material = node.child(MATERIAL_TAG.c_str()); material;
       material = material.next_sibling(MATERIAL_TAG.c_str()))
  {
    if(!this->read(material, name, label, color)) return false;
    materials->AddOrUpdateMaterial(name, label, color);
  }
  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, QString & name, QString & label, QColor & color)
{
  if(!read(node, COLOR_TAG.c_str(), color)) return false;
  if(!read(node, NAME_TAG.c_str(), name)) return false;
  if(!read(node, LABEL_TAG.c_str(), label)) return false;

  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, PinCell * dc, cmbNucMaterialColors * materials)
{
  if( dc == NULL ) return false;
  bool r = true;
  QString name, label;
  QColor color;
  r &= read(node, NAME_TAG.c_str(), name);
  r &= read(node, LABEL_TAG.c_str(), label);
  r &= read(node, LEGEND_COLOR_TAG.c_str(), color);

  std::string materialName;
  if(read(node, MATERIAL_TAG.c_str(), materialName))
  {
    dc->setCellMaterial(materials->getMaterialByName(materialName.c_str()));
  }
  dc->setName(name);
  dc->setLabel(label);
  dc->SetLegendColor(color);

  for(pugi::xml_node tnode = node.child(CYLINDER_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(CYLINDER_TAG.c_str()))
  {
    Cylinder * c = new Cylinder(0,0,0);
    r &= this->read(tnode, c, materials);
    dc->AddPart(c);
  }

  double junk[2] = {0,0};
  for(pugi::xml_node tnode = node.child(FRUSTRUM_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(FRUSTRUM_TAG.c_str()))
  {
    Frustum * f = new Frustum(junk, 0, 0);
    r &= this->read(tnode, f, materials);
    dc->AddPart(f);
  }

  return r;
}

bool xmlHelperClass::read(pugi::xml_node & node, Cylinder * dc, cmbNucMaterialColors * materials)
{
  if( dc == NULL ) return false;
  bool r = true;
  r &= read(node, RADIUS_TAG.c_str(), dc->r);
  r &= readPSP(node, dc, materials);
  return r;
}

bool xmlHelperClass::read(pugi::xml_node & node, Frustum * dc, cmbNucMaterialColors * materials)
{
  if( dc == NULL ) return false;
  bool r = true;
  r &= read(node, RADIUS_TAG.c_str(), dc->r, 2);
  r &= readPSP(node, dc, materials);
  return r;
}

bool xmlHelperClass::readPSP(pugi::xml_node & node, PinSubPart * dc,
                             cmbNucMaterialColors * materials)
{
  if( dc == NULL ) return false;
  QString str;
  if(!this->read(node, LOC_TAG.c_str(), str)) return false;
  QStringList l = str.split(",");
  dc->x = l.value(0).toDouble();
  dc->y = l.value(1).toDouble();
  dc->setZ1(l.value(2).toDouble());
  dc->setZ2(l.value(3).toDouble());
  int i = 0;
  for(pugi::xml_node tnode = node.child(MATERIAL_LAYER_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(MATERIAL_LAYER_TAG.c_str()))
  {
    cmbNucMaterialLayer * ml = new cmbNucMaterialLayer();
    if(!read(tnode, ml, materials)) return false;
    dc->setMaterialLayer(i++, ml);
  }
  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, cmbNucMaterialLayer * ml,
                          cmbNucMaterialColors * materials)
{
  bool r = true;
  QString materialName;
  r &= read(node, THICKNESS_TAG.c_str(), ml->getThickness(), 2);
  r &= read(node, MATERIAL_TAG.c_str(), materialName);
  QPointer<cmbNucMaterial> cnm = materials->getMaterialByName(materialName);
  ml->changeMaterial(cnm);

  return r;
}

template <class TYPE>
bool xmlHelperClass::read(pugi::xml_node & node, cmbNucPartLibrary * dl,
                          cmbNucMaterialColors * materials)
{
  if(dl == NULL) return false;

  TYPE * tmpPart = NULL;
  std::string tag = getTag(tmpPart);

  for(pugi::xml_node tn = node.child(tag.c_str()); tn; tn = tn.next_sibling(tag.c_str()))
  {
    TYPE * part  = new TYPE();
    if(!this->read(tn, part, materials))
    {
      delete part;
      return false;
    }
    boost::function<bool (QString)> fun = boost::bind(&cmbNucPartLibrary::nameConflicts, dl, _1);
    part->setName(cmbNucPart::getUnique(fun, part->getName(), true, false));
    fun = boost::bind(&cmbNucPartLibrary::labelConflicts, dl, _1);
    part->setLabel(cmbNucPart::getUnique(fun, part->getLabel(), true, false));
    dl->addPart(part);
  }

  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, DuctCell * dc,
                          cmbNucMaterialColors * materials)
{
  if(dc == NULL) return false;
  QString name;
  if(!this->read(node, NAME_TAG.c_str(), name)) return false;
  dc->setName(name);
  dc->setLabel(name);
  for(pugi::xml_node tnode = node.child(DUCT_LAYER_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(DUCT_LAYER_TAG.c_str()))
  {
    Duct * d = new Duct(0,0,0);
    if(!read(tnode, d, materials)) return false;
    dc->AddDuct(d);
  }
  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, Duct * dc,
                          cmbNucMaterialColors * materials)
{
  if(dc == NULL) return false;
  QString str;
  if(!read(node, LOC_TAG.c_str(), str)) return false;
  QStringList l = str.split(",");

  dc->setX(l.value(0).toDouble());
  dc->setY(l.value(1).toDouble());

  dc->setZ1(l.value(2).toDouble());
  dc->setZ2(l.value(3).toDouble());

  double thickness[2];
  if(!read(node, THICKNESS_TAG.c_str(), thickness, 2)) return false;
  dc->setThickness(0, thickness[0]);
  dc->setThickness(1, thickness[1]);

  int i = 0;
  for(pugi::xml_node tnode = node.child(MATERIAL_LAYER_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(MATERIAL_LAYER_TAG.c_str()))
  {
    cmbNucMaterialLayer * ml = new cmbNucMaterialLayer();
    if(!read(tnode, ml, materials)) return false;
    dc->setMaterialLayer(i++, ml);
  }

  return true;
}

bool xmlHelperClass::readAssyPart(pugi::xml_node & node, cmbNucPart* part)
{
  QString tmpQ, label;
  if(!read(node, LABEL_TAG.c_str(), label)) return false;
  if(!read(node, NAME_TAG.c_str(), tmpQ))
  {
    tmpQ = "assembly_" + label;
  }
  part->setLabel(label);
  part->setName(tmpQ);
  QColor color;
  if(!read(node, LEGEND_COLOR_TAG.c_str(), color)) return false;
  part->SetLegendColor(color);
  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, cmbNucAssembly * assy)
{
  QString tmpQ, assy_label;
  std::string tmp;
  if(!readAssyPart(node, assy)) return false;
  if(!read(node, DUCT_TAG.c_str(), tmpQ)) return false;
  cmbNucPartLibrary * dl = assy->getDuctLibrary();
  DuctCell * d = dl->GetPart<DuctCell>(tmpQ, cmbNucPartLibrary::BY_NAME);
  assy->setDuctCell(d);
  if(!read(node, GEOMETRY_TAG.c_str(), tmp)) return false;
  assy->setGeometryLabel(tmp);
  bool iac;
  if(!read(node, CENTER_PINS_TAG.c_str(), iac)) return false;
  assy->setCenterPins(iac);
  double pitch[2];
  if(!read(node, PITCH_TAG.c_str(), pitch, 2)) return false;
  assy->setPitch(pitch[0], pitch[1], false);

  double degree;
  if(read(node, ROTATE_TAG.c_str(), degree))
  {
    assy->setZAxisRotation(degree);
  }

  pugi::xml_node paramNode = node.child(PARAMETERS_TAG.c_str());
  {
    cmbAssyParameters * params = assy->GetParameters();

    //Other Parameters
    read(paramNode, MOVE_TAG.c_str(), params->getMoveXYZ(), 3);
    {
      std::string c;
      read(paramNode, CENTER_TAG.c_str(), c);
      if(c != "NotSet")
      {
        params->setCenter(c);
      }
    }

    {
      bool v = false;
      if(read(paramNode, "save_exodus", v)) params->setSave_Exodus(v);
    }
#define FUN_SIMPLE(TYPE,X,Var,Key)\
    {\
      TYPE v;\
      if(read(paramNode, #Key, v)) params->set##Var(v);\
    }
    ASSYGEN_EXTRA_VARABLE_MACRO()
    FUN_SIMPLE(double,      QString, AxialMeshSize,            AxialMeshSize)
#undef FUN_SIMPLE

    int tmpI;
    if(read(paramNode, "materialset_startid", tmpI)) params->setMaterialSet_StartId(tmpI);
    if(read(paramNode, "neumannset_startid", tmpI)) params->setNeumannSet_StartId(tmpI);

    QString unknown;
    for(pugi::xml_node tnode = node.child(UNKNOWN_TAG.c_str()); tnode;
        tnode = tnode.next_sibling(UNKNOWN_TAG.c_str()))
    {
      std::string tmp_str;
      if(read(tnode, STR_TAG.c_str(), tmp_str))
        unknown.append((tmp_str + "\n").c_str());
    }
    params->setUnknownParams(unknown);
  }

  pugi::xml_node lnode = node.child(LATTICE_TAG.c_str());
  if(!readLattice( lnode, *assy )) return false;

  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, cmbNucCore::boundaryLayer * bl,
                          cmbNucMaterialColors * materials)
{
  bool r = true;
  std::string materialName;
  r &= read(node, MATERIAL_TAG.c_str(), materialName);
  bl->interface_material = materials->getMaterialByName(materialName.c_str());
  r &= read(node, THICKNESS_TAG.c_str(), bl->Thickness);
  r &= read(node, BIAS_TAG.c_str(), bl->Bias);
  r &= read(node, INTERVAL_TAG.c_str(), bl->Intervals);
  return r;
}

bool xmlHelperClass::read(pugi::xml_node & node, cmbNucAssemblyLink * link, cmbNucCore & core)
{
  QString tmp, label;
  bool r = readAssyPart(node, link);
  r &= read(node, ASSEMBLY_TAG.c_str(), tmp);
  cmbNucAssembly * a = dynamic_cast<cmbNucAssembly*>(core.getFromLabel(tmp));
  if(a == NULL) return false;
  link->setLink(a);
  int tmpI = 0;
  r &= read(node, "MaterialStartID", tmpI);
  link->setMaterialStartID(tmpI);
  r &= read(node, "NeumannStartID", tmpI);
  link->setNeumannStartID(tmpI);
  return r;

}

bool xmlHelperClass::readLattice(pugi::xml_node & node, LatticeContainer & container)
{
  unsigned int type;
  Lattice & lattice = container.getLattice();
  if(!read(node, TYPE_TAG.c_str(), type)) return false;
  int subtype;
  if(!read(node, SUB_TYPE_TAG.c_str(), subtype)) return false;

  lattice.SetGeometryType(static_cast<enumGeometryType>(type));
  lattice.SetGeometrySubType(subtype);

  std::vector< std::vector< QString > > grid;
  QString sgrid;
  if(!read(node, GRID_TAG.c_str(), sgrid)) return false;
  QStringList rs = sgrid.split(";");

  for(int i = 0; i < rs.size(); ++i)
  {
    QString & t = rs[i];
    if(t.isEmpty()) continue;
    size_t at = grid.size();
    grid.resize(grid.size() + 1);
    std::vector<QString> & v = grid[at];
    QStringList tl = t.split(",");
    for(int j = 0; j < tl.size(); ++j)
    {
      v.push_back(tl[j]);
    }
  }

  size_t xs = grid.size();
  if(xs == 0) return false;
  size_t ys = grid[0].size();
  lattice.SetDimensions(static_cast<int>(xs),  static_cast<int>(ys), true);
  for(unsigned int i = 0; i < grid.size(); ++i)
  {
    for(unsigned int j = 0; j < grid[i].size(); ++j)
    {
      lattice.SetCell( i, j, container.getFromLabel(grid[i][j]), grid[i][j] != "");
    }
  }

  container.setUpdateUsed();
  return true;
}

bool xmlHelperClass::read(pugi::xml_node & node, std::string attName,
                          std::vector<cmbNucCoreParams::NeumannSetStruct> & out)
{
  bool r = true;
  pugi::xml_node nnode = node.child(attName.c_str());
  for(pugi::xml_node tnode = nnode.child(NEUMANN_VALUE_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(NEUMANN_VALUE_TAG.c_str()))
  {
    cmbNucCoreParams::NeumannSetStruct nss;
    r &= read(tnode, SIDE_TAG.c_str(), nss.Side);
    r &= read(tnode, ID_TAG.c_str(), nss.Id);
    r &= read(tnode, EQUATION_TAG.c_str(), nss.Equation);
    out.push_back(nss);
  }
  return r;
}

bool xmlHelperClass::read(pugi::xml_node & node, std::string attName,
                           cmbNucCoreParams::ExtrudeStruct & es)
{
  bool r = true;
  pugi::xml_node ttnode = node.child(attName.c_str());
  double s;
  int d;
  r &= read(ttnode, SIZE_TAG.c_str(), s);
  r &= read(ttnode, DIVISIONS_TAG.c_str(), d);
  if(r)
  {
    es.setSize(s);
    es.setDivisions(d);
  }
  return r;
}

//////////////////////////////////read write static functions////////////////////////////////
bool xmlFileReader::read(std::string fname, cmbNucCore & core)
{
  std::ifstream in(fname.c_str(), std::ios::in);
  if (!in)
  {
    return false;
  }

  // Allocate string
  std::string content;
  in.seekg(0, std::ios::end);
  content.resize(in.tellg());

  // Read file
  in.seekg(0, std::ios::beg);
  in.read(&content[0], content.size());
  in.close();

  xmlHelperClass helper;

  core.setFileName(fname);

  return helper.read(content, core);
}

bool xmlFileReader::read(std::string fname, std::vector<PinCell*> & pincells,
                         cmbNucMaterialColors * materials)
{
  xmlHelperClass helper;

  pugi::xml_document document;
  if(!helper.openReadFile(fname,document)) return false;

  pugi::xml_node node = document.child(CORE_TAG.c_str()).child(MATERIALS_TAG.c_str());
  if(!helper.read(node, materials)) return false;

  node = document.child(CORE_TAG.c_str()).child(PINS_TAG.c_str());

  for (pugi::xml_node pnode = node.child(PINCELL_TAG.c_str()); pnode;
       pnode = pnode.next_sibling(PINCELL_TAG.c_str()))
  {
    PinCell * pc = new PinCell();
    if(!helper.read(pnode, pc, materials)) return false;
    pincells.push_back(pc);
  }
  return true;
}

bool xmlFileReader::read(std::string fname, std::vector<DuctCell*> & ductcells,
                         cmbNucMaterialColors * materials)
{
  xmlHelperClass helper;

  pugi::xml_document document;
  if(!helper.openReadFile(fname,document)) return false;

  pugi::xml_node node = document.child(CORE_TAG.c_str()).child(MATERIALS_TAG.c_str());
  if(!helper.read(node, materials)) return false;

  node = document.child(CORE_TAG.c_str()).child(DUCTS_TAG.c_str());

  for (pugi::xml_node pnode = node.child(DUCT_CELL_TAG.c_str()); pnode;
       pnode = pnode.next_sibling(DUCT_CELL_TAG.c_str()))
  {
    DuctCell * dc = new DuctCell();
    if(!helper.read(pnode, dc, materials)) return false;
    ductcells.push_back(dc);
  }
  return true;
}

bool xmlFileReader::read(std::string fname, std::vector<cmbNucAssembly*> & assys,
                         cmbNucPartLibrary * pl, cmbNucPartLibrary * dl,
                         cmbNucMaterialColors * materials)
{
  if(pl == NULL) return false;
  if(dl == NULL) return false;
  xmlHelperClass helper;

  pugi::xml_document document;
  if(!helper.openReadFile(fname,document)) return false;

  pugi::xml_node rnode = document.child(CORE_TAG.c_str());

  {
    pugi::xml_node node = rnode.child(MATERIALS_TAG.c_str());
    if(!helper.read(node, materials)) return false;
  }

  {
    pugi::xml_node node = rnode.child(DUCTS_TAG.c_str());
    if(!helper.read<DuctCell>(node, dl, materials)) return false;
  }

  {
    pugi::xml_node node = rnode.child(PINS_TAG.c_str());
    if(!helper.read<PinCell>(node, pl, materials)) return false;
  }

  for(pugi::xml_node tnode = rnode.child(ASSEMBLY_TAG.c_str()); tnode;
      tnode = tnode.next_sibling(ASSEMBLY_TAG.c_str()))
  {
    cmbNucAssembly* assy = new cmbNucAssembly("","", Qt::white);
    assy->setPinLibrary(pl);
    assy->setDuctLibrary(dl);
    if(!helper.read(tnode, assy)) return false;
    assys.push_back(assy);
  }
  return true;
}


bool xmlFileWriter::write(std::string fname, cmbNucCore & core, bool /*updateFname*/) //TODO the file update
{
  std::string out;
  xmlHelperClass helper;
  if(helper.writeToString(out, core) && helper.writeStringToFile(fname, out))
  {
    core.setAndTestDiffFromFiles(false);
    return true;
  }
  return false;
}

