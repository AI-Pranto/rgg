#include "cmbNucDefaults.h"

cmbNucDefaults::cmbNucDefaults()
{
}

cmbNucDefaults::~cmbNucDefaults()
{
}
#define HELPER(X)                   \
bool cmbNucDefaults::has##X() const \
{                                   \
  return X.valid;                   \
}                                   \
void cmbNucDefaults::clear##X()     \
{                                   \
  X.valid = false;                  \
}

#define FUN1(T, X)                   \
HELPER(X)                            \
void cmbNucDefaults::set##X(T vin)   \
{                                    \
  X.set(vin);                        \
}                                    \
T cmbNucDefaults::get##X()           \
{                                    \
  return X.X;                        \
}

EASY_DEFAULT_PARAMS_MACRO()

#undef FUN1
