#ifndef __cmbCoreParametersWidget_h
#define __cmbCoreParametersWidget_h

#include <QFrame>
#include "cmbNucPartDefinition.h"
#include "cmbNucWidgetChangeCheckingData.h"
#include <QStringList>

class cmbNucCore;

class cmbCoreParametersWidget : public QFrame
{
  Q_OBJECT

public:
  cmbCoreParametersWidget(QWidget* p);
  virtual ~cmbCoreParametersWidget();

  // Description:
  // set/get the Core that this widget with be interact with
  void setCore(cmbNucCore*, cmbNucWidgetChangeChecker & checker);
  cmbNucCore* getCore(){return this->Core;}

signals:
  void set(QString v);
  void set(bool b);
  void valuesChanged();

private:
  class cmbCoreParametersWidgetInternal;
  cmbCoreParametersWidgetInternal* Internal;
  cmbNucCore *Core;
};
#endif
