#ifndef __DrawLatticeItem_h
#define __DrawLatticeItem_h

#include <QGraphicsPolygonItem>
#include <QObject>

#include "cmbNucLattice.h"

class DrawLatticeItem : public QGraphicsPolygonItem
{
  typedef QGraphicsPolygonItem Superclass;

public:
  DrawLatticeItem(const QPolygonF& polygon, int layer, int cellIdx,
                  Lattice::CellReference const& ref, QGraphicsItem* parent = 0);

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

  QString text() const;

  int layer();
  int cellIndex();

  cmbNucPart * getPart();

  void select()
  {
    refCell.setDrawMode(Lattice::CellReference::SELECTED);
  }

  bool checkRadiusIsOk(double r) const
  {
    return !refCell.radiusConflicts(r);
  }

  QPointF getCentroid() const;

protected:
  void drawText(QPainter* painter);

private:
  Lattice::CellReference const& refCell;
  int m_layer;
  int m_cellIndex;
  QPointF localCenter;
};

#endif
