#ifndef __cmbNucDefaultWidget_h
#define __cmbNucDefaultWidget_h

#include <QWidget>
#include "cmbNucPartDefinition.h"
#include "cmbNucWidgetChangeChecker.h"
#include <QPointer>

class cmbNucDefaultWidgetInternal;
class cmbNucDefaults;
class cmbNucCore;

class cmbNucDefaultWidget : public QWidget
{
  Q_OBJECT

public:
  cmbNucDefaultWidget(QWidget *parent = 0);
  ~cmbNucDefaultWidget();
  bool assyPitchChanged();
  bool needCameraReset();
public slots:
  void set(cmbNucCore * core, cmbNucWidgetChangeChecker & checker);
  void reset();
signals:
  void commonChanged();
  void widthChanged(double);
  void heightChanged(double);
protected:
  QPointer<cmbNucDefaults> Current;
  cmbNucDefaultWidgetInternal * Internal;
};
#endif
